<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

 Route::get('/', function () {
     return view('beranda');
 });



Route::get('/beranda', 'BerandaController@index');


Route::get('/kontak', 'KontakController@index');


Route::auth();
Route::get('/', 'HomeController@Welcome');
Route::get('/home', 'HomeController@index');



Route::get('/admin', 'AdminController@index');
Route::post('/admin/save', 'AdminController@store');
Route::get('/admin/create', 'AdminController@create');
Route::get('/admin/detail/{id}', 'AdminController@show');
Route::get('/admin/edit/{id}', 'AdminController@edit');
Route::get('/admin/hapus/{id_keuangan}', 'AdminController@destroy');



Route::get('/produk', 'ProdukController@index');

Route::get('/produk/show/{id}', 'ProdukController@show');

Route::get('/pengajuan/delete/{id}', 'PengumumanController@destroy');

Route::get('/pengajuan/create', 'ProdukController@create');

Route::post('/produk/save', 'ProdukController@store');

Route::get('/produk/delete/{id_produk}', 'ProdukController@destroy');

Route::post('/produk/upload/save/{id}', 'ProdukController@uploadSave');

Route::get('/komentar/vote/{id}/{type}', 'ProdukController@updateVote');





Route::get('/uploadadmin', 'AdminController@upload');

Route::post('admin/upload/save', 'AdminController@upload_save');
