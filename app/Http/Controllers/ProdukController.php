<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Komentar;


class ProdukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $produk = Produk::all();
        return view('produk')
        ->with("produk", $produk);
    }


    public function show($id_produk)
    {
        $produk = Produk::where('id_produk', '=', $id_produk)->first();
        $komentar = Komentar::where('id_produk', '=', $id_produk)->get();
        return view('pengajuan')->with('produk', $produk)->with('komentar', $komentar);
    }
    

    public function destroy($id_produk)
    {
        Produk::where('id_produk', '=', $id_produk)->delete();
        return redirect('/');
    }
    

    public function create()
    {
        $produk = Produk::all();
        return view('form.form_pengajuan_create')->with('produk', $produk);
    }
    
    public function store(Request $request)
    {
        $simpan = array(
            'merk' => $request['merk'],
            'model' => $request['model'],
            'bahan_bakar' => $request['bahan_bakar'],
            'tahun_pembuatan' => $request['tahun_pembuatan'],
        );
        $produk = Produk::insertGetId($simpan);
        $produk = Produk::where('id_produk', '=', $produk)->first();
        return view('upload_produk')->with('produk', $produk);
    }

    public function uploadSave(Request $request, $id_produk)
    {
        if ($request->hasFile('foto'))
        {
            $request->file();
            $fileName = 'produk_photo_with_id' .$id_produk.'_date_'. date('d_m_y') . '.jpg';
            $destinationPath = public_path().'/assets';
            $request->file('foto')->move($destinationPath, $fileName);
            $inputimg = array('foto' => $fileName);
            Produk::where('id_produk', '=', $id_produk)->update($inputimg);
        }
        $produk = Produk::where('id_produk', '=', $id_produk)->first();
        return view('/pengajuan')
            ->with('produk', $produk);
    }


    public function saveKomentar(Request $request, $id_produk)
    {
        $simpan = array(
            'isi' => $request['isi'],
            'disvote' => 0,
            'vote' => 0,
            'id_produk' => $id_produk
        );

        komentar::insertGetId($simpan);
        $produk = Produk::where('id_produk', '=', $id_produk)->first();
        $komentar = Komentar::where('id_produk', '=', $id_produk)->get();
        return view('pengajuan')->with('produk', $produk)->with('komentar', $komentar);
    }


    public function updateVote($id_komentar, $type)
    {
        $type;
        $komentar = Komentar::where('id_komentar', '=', $id_komentar)->first();
        $new_vote = $komentar->upvotes + 1;

        if ($type == 1){
            $simpan = array(
                'upvotes' => $new_vote,
            );
            Komentar::where('id_komentar', $id_komentar)->update($simpan);
        }
        elseif ($type == 2) {
            $simpan = array(
                'downvotes' => $new_vote,
            );
            Komentar::where('id_komentar', $id_komentar)->update($simpan);
        }

        $produk = Produk::where('id_produk', '=', $komentar->id_produk)->first();
        $komentar = Komentar::where('id_produk', '=', $komentar->id_produk)->get();
        return view('pengajuan')->with('produk', $produk)->with('komentar', $komentar);
    }
}