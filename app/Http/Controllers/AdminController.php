<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use App\Http\Requests;
use App\Ketua;
use app\Keuangansiswa;
use app\Keuangan;
use App\Siswa;
use App\Kelas;
use app\Guru;
use App\Admin;
use App\User;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $admin = Admin::all();            
        return view('admin')
        ->with("admin", $admin);
    }
    
    public function show($id)
    {
        $user = User::where('id', '=', $id)->first();
        return view('userdetail')->with('user', $user);
    }
    
    public function destroy($id_admin)
    {
        $admin = Admin::where('id_admin', '=', $id_admin)->delete();
        return redirect('admin');
    }


    public function create()
    {
        $admin = Admin::all();
        return view('form.form_admin_create')->with('admin', $admin);
    }
    


    public function store(Request $request)
    {
        $simpan = array(
        'nama' => $request['nama'],
        'jenis_kel' => $request['jenis_kel'],
        'jabatan' => $request['jabatan'],
        'alamat' => $request['alamat'],
        'telepon' => $request['telepon'],
        'foto' => $request['foto'],
        );

        $admin = Admin::insertGetId($simpan);

        User::create([
            'name' => $request['nama'],
            'email' => $request['email'],
            'password' => bcrypt($request['telepon']), 
            'user_type' => 'admin',
            'mapel_id' => '0',
            'guru_id' => '0',
            'siswa_id' => '0',
            'id_kurikulum' => '0',
            'id_kepsek' => '0',
            'id_admin' => $admin,
       ]);

        return redirect('admin');
    }


    public function edit($id_admin)
    {
        $admin = Admin::where('id_admin', $id_admin)->first();
        return view('form.form_admin_edit')
        ->with('admin', $admin);
    }

    public function update(Request $request)
    {
        $simpan = array(
        'nama' => $request['nama'],
        'jenis_kel' => $request['jenis_kel'],
        'jabatan' => $request['jabatan'],
        'alamat' => $request['alamat'],
        'telepon' => $request['telepon'],
        'foto' => $request['foto'],
        );
        
        //query masukan data
        $admin = Admin::where('id_admin', $request['id_admin'])->update($simpan);
        
        return redirect('admin');
    }



    public function upload()
    {
        return view('uploadadmin');
    }



    public function upload_save(Request $request)
    {
        $getid = auth()->user()['id_admin'];

        if ($request->hasFile('foto')) 
        {
            $image = $request->file(); 
            $fileName = 'admin_photo_with_id' . $getid.'_date_'. date('d_m_y') . '.jpg'; 
            $destinationPath = public_path().'/assets/foto/admin/'; 
            $process = $request->file('foto')->move($destinationPath, $fileName);
            $inputimg = array('foto' => $fileName,);

            $rs = Admin::where('id_admin', '=', $getid)->update($inputimg);
        }

        $message = "Foto Anda Berhasil Di Upload"; 
        return redirect('/')->with('message', $message);
    }

}
