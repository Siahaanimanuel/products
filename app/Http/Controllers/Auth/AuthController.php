<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Siswa;
use App\Anggota;
use Validator;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed', 
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
//        $getid = $data['no_ktp'];
//
//        if ($data->hasFile('foto'))
//        {
////            $data->file();
//            $fileName = 'anggota_photo_with_id' .$getid.'_date_'. date('d_m_y') . '.jpg';
//            $destinationPath = public_path().'/assets/foto/anggota/';
//            $data->file('foto')->move($destinationPath, $fileName);
//        }

        $simpan = array(
            'anggota_id' => $data['no_ktp'],
            'nama_depan' => $data['name'],
            'nama_belakang' => $data['nama_belakang'],
            'talenta' => $data['talenta'],
            'jenis_kelamin' => $data['jenis_kelamin'],
            'perguruan_tinggi' => $data['perguruan_tinggi'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tgl_lahir' => $data['tgl_lahir'],
            'alamat1' => $data['alamat1'],
            'alamat2' => $data['alamat2'],
            'kode_pos' => $data['kode_pos'],
            'kota' => $data['kota'],
            'provinsi' => $data['provinsi'],
            'instagram' => $data['instagram'],
            'gereja' => $data['gereja'],
            'status' => '0'
        );

        //query masukan data
        Anggota::insertGetId($simpan);
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
			'user_type' => 'anggota',
            'anggota_status' => '0',
            'anggota_foto' => '0',
            'id_anggota' => $data['no_ktp'],
        ]);

        return $user;
    }
}
