<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Kalender;

class BerandaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $kalender = Kalender::all();
        return view('beranda')
        ->with("kalender", $kalender);
    }
	
	public function show($id)
	{
	
	}
	
	public function destroy($id)
	{

	}
}
