<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Admin;
use App\Produk;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$nama="Nuel";
		$a=1;
		$b=2;
		$tambah=$a+$b;
		$user=User::all();
		$name = auth()->user()['name'];
		
        return view('home')
		->with("nama", $nama)
		->with("tambah", $tambah)
		->with("user", $user);
    }

    public function Welcome()
    {
    	# mendapatkan id users yg login
        $getID_admin = auth()->user()['id_admin'];

        $produk = Produk::all();

        	return view('welcome')
            ->with("produk", $produk);
        if (isset($getID_admin) == true AND $getID_admin != 0) {
            $detail_admin = Admin::where('admin.id_admin', '=', $getID_admin)->first()
            ->with("produk", $produk);
        }
        
    }

	
	public function show($id)
	{
		$user = User::where('id', '=', $id)->first();
		return view('userdetail')->with('user', $user);
	}
	
	public function destroy($id)
	{
		$user = User::where('id', '=', $id)->delete();
		return redirect('home');
	}
}
