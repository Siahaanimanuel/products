@extends('layouts.app')

@section('content')
<style>
   h3{
      text-align:center; }
   #coba { 
      border-collapse:collapse;
      border-spacing:0;     
      font-family:Arial, sans-serif;
      font-size:16px;
      padding-left:300px;
      margin:auto; }
   #cobath {
      font-weight:bold;
      padding:10px;
      color:#fff;
      background-color:#2A72BA;
      border-top:1px black solid;
      border-bottom:1px black solid;}
   #cobatd {
      padding:10px;
      border-top:1px black solid;
      border-bottom:1px black solid;
      text-align:center; }         
   #cobatr:nth-child(even) {
     background-color: #DFEBF8; }
    h3{
        text-align:center; }
    table {
        border-collapse:collapse;
        border-spacing:0;
        font-family:Arial, sans-serif;
        font-size:13px;
        padding-left:300px;
        margin:auto; }
    th {
        font-weight:bold;
        padding:10px;
        color:#fff;
        background-color:#2A72BA;
        border-top:1px black solid;
        border-bottom:1px black solid;}
    td {
        padding:10px;
        border-top:1px black solid;
        border-bottom:1px black solid;
        text-align:left; }
    tr:nth-child(even) {
        background-color: #DFEBF8; }
</style>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><center><b>Selamat Datang di Sistem Pengelolaan Produk</b></center></div>

                <div class="panel-body">
                    @if(Auth::user()->user_type == 'admin')
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-md-offset-0">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Data Produk</div>
                                        <br>
                                        <div class="panel-body">

                                            <div class="table table-responsive">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Merk</th>
                                                        <th>Model</th>
                                                        <th>Bahan Bakar</th>
                                                        <th>Tahun</th>
                                                        <th>Pilihan</th>
                                                    </tr>

                                                    @if(count($produk) > 0)
                                                        @foreach($produk as $b)
                                                            <tr>
                                                                <td>{{$b['merk']}}</td>
                                                                <td>{{$b['model']}}</td>
                                                                <td>{{$b['bahan_bakar']}}</td>
                                                                <td>{{$b['tahun_pembuatan']}}</td>

                                                                <td><a href="{{url('/produk/show/'.$b['id_produk'])}}" class="btn btn-default btn-xs">Lihat</a>
                                                                    <a href="{{url('/produk/edit/'.$b['id_produk'])}}" class="btn btn-default btn-xs">Edit</a>
                                                                    <a href="{{url('/produk/delete/'.$b['id_produk'])}}" class="btn btn-default btn-xs">Hapus</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td>Tidak Ada Pengumuman</td>
                                                        </tr>
                                                    @endif
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   @endif
                </div>
            </div>
        </div>
    </div>
@endsection