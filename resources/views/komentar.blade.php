@extends('layouts.app')

@section('content')
    <style>
        h3{
            text-align:center; }
        #coba {
            border-collapse:collapse;
            border-spacing:0;
            font-family:Arial, sans-serif;
            font-size:16px;
            padding-left:300px;
            margin:auto; }
        #cobath {
            font-weight:bold;
            padding:10px;
            color:#fff;
            background-color:#2A72BA;
            border-top:1px black solid;
            border-bottom:1px black solid;}
        #cobatd {
            padding:10px;
            border-top:1px black solid;
            border-bottom:1px black solid;
            text-align:center; }
        #cobatr:nth-child(even) {
            background-color: #DFEBF8; }
        h3{
            text-align:center; }
        th {
            font-weight:bold;
            padding:10px;
            color:#fff;
            background-color:#2A72BA;
            border-top:1px black solid;
            border-bottom:1px black solid;}
        td {
            padding:10px;
            text-align:left; }
    </style>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><center><b>Selamat Datang di Sistem Pengelolaan Produk</b></center></div>

                <div class="panel-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-0 col-md-0 col-md-0">
                                <div class="well well-sm">
                                    <div class="row">
                                        <div class="col-sm-4 col-md-4">
                                            <img src={{url('assets/'.$produk['foto'])}} alt="" class="img-rounded img-responsive" />
                                            <form action="{{ url('/uploadproduk') }}" target="blank">
                                                <br>
                                            </form>
                                        </div>
                                        <div class="col-sm-6 col-md-8">
                                            <table>
                                                <tr>
                                                    <td>Merk</td><td>:&nbsp;</td><td>{{$produk['merk']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Type</td><td>:&nbsp;</td><td>{{$produk['model']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Bahan Bakar</td><td>:&nbsp;</td><td>{{$produk['bahan_bakar']}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Bahan Bakar</td><td>:&nbsp;</td><td>{{$produk['tahun_pembuatan']}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection