@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Buat Pengajuan</div>
				<div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('produk/save') }}">
                        {{ csrf_field() }}
                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Merk</label>
                            <div class="col-md-6">
                                <input id="merk" type="text" class="form-control" name="merk" placeholder='masukan merk mobil'>
                            </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Model</label>
                                <div class="col-md-6">
                                    <input id="model" type="text" class="form-control" name="model" placeholder='masukan model mobil'>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Bahan Bakar</label>
                                <div class="col-md-6">
                                    <select name="bahan_bakar">
                                        <option value="bensin">Bensin</option>
                                        <option value="solar">Solar</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Tahun Pembuatan</label>
                                <div class="col-md-6">
                                    <select name="tahun_pembuatan">
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                         save
                                    </button>
                                </div>
                            </div>
                    </form>
                </div>
				
            </div>
        </div>
    </div>
</div>
@endsection
