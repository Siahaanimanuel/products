@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><center><b>Tambah Admin Pengurus</b></center></div>
				<div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/save') }}">
                        {{ csrf_field() }}
							

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Nama Pegawai</label>
                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" placeholder='masukan Nama Pegawai'>
                            </div>
                            </div>


                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" placeholder='masukan Email'>
                            </div>
                            </div>




                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Jabatan</label>
                            <div class="col-md-6">
                                <input id="jabatan" type="text" class="form-control" name="jabatan" placeholder='masukan Posisi Jabatan'>
                            </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Jenis Kelamin</label>
                                <div class="col-md-6">
                                    <input id="jenis_kel" type="text" class="form-control" name="jenis_kel" placeholder='masukan Jenis Kelamin'>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Telepon</label>
                                <div class="col-md-6">
                                    <input id="telepon" type="text" class="form-control" name="telepon" placeholder='Masukan Nomor Telepon'>
                                </div>
                            </div>



                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Alamat</label>
                            <div class="col-md-6">
                                <input id="alamat" type="text" class="form-control" name="alamat" placeholder='masukan Alamat Pegawai'>
                            </div>
                            </div>
							

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                     save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
				
            </div>
        </div>
    </div>
</div>
@endsection
