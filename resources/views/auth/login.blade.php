<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Proct Management</title>
  <link rel="shortcut icon" href="//">
  <link rel="stylesheet" href="assets/css/style.css">

</head>
<body  style="background-color:#366d8c;">
  <section class="container">
    <div class="login">
          <h1>LOGIN</h1>
        

      <form action="" class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
      {{ csrf_field() }}
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
      <p><input  id="email" type="text" class="form-control" name="email" value="" value="{{ old('email') }}" placeholder="Username or Email"></p>
        @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <p><input id="password" type="password" class="form-control" name="password" placeholder="Password"></p>
         @if ($errors->has('password'))
            <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
            </span>
         @endif
    </div>
        <p class="remember_me">
          <label>
            <input type="checkbox" name="remember" id="remember_me">
            Ingatkan Saya
          </label>
        </p>
        <p class="submit"><input type="submit" class="btn btn-primary" name="commit" value="Login"></p>
      </form>
    </div>

    <div class="login-help">
      <p>Lupa Password ?&nbsp;&nbsp;&nbsp;<a class="btn btn-link" href="{{ url('/password/reset') }}">Klik Disini Untuk Merubah</a></p>
    </div>
  </section>
</body>
</html>