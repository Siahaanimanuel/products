@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><center><b>Hi, Selamat Datang, Ayo Daftar Sebagai Anggota PMKRI Jakarta Pusat</b></center></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nama Depan</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nama Belakang</label>

                                <div class="col-md-6">
                                    <input id="nama_belakang" type="text" class="form-control" name="nama_belakang" placeholder="Masukkan Nama">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">No. KTP</label>

                                <div class="col-md-6">
                                    <input id="no_ktp" type="text" class="form-control" name="no_ktp" placeholder="no ktp">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">Talenta</label>

                                <div class="col-md-6">
                                    <input id="talenta" type="talenta" class="form-control" name="talenta" placeholder="talenta">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Jenis Kelamin</label>
                                <div class="col-md-6">
                                    <select name="jenis_kelamin">
                                        <option value="laki_laki">Laki-Laki</option>
                                        <option value="perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="perguruan_tinggi" class="col-md-4 control-label">Perguruan Tinggi</label>

                                <div class="col-md-6">
                                    <input id="perguruan_tinggi" type="perguruan_tinggi" class="form-control" name="perguruan_tinggi" placeholder="perguruan_tinggi">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="tempat_lahir" class="col-md-4 control-label">Tempat lahir</label>

                                <div class="col-md-6">
                                    <input id="tempat_lahir" type="tempat_lahir" class="form-control" name="tempat_lahir" placeholder="tempat_lahir">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Tanggal Lahir</label>
                                <div class="col-md-6">
                                    <input type="date" class="form-control" name="tgl_lahir" placeholder=''>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="alamat1" class="col-md-4 control-label">Alamat</label>

                                <div class="col-md-6">
                                    <input id="alamat1" type="alamat1" class="form-control" name="alamat1" placeholder="alamat1">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="alamat2" class="col-md-4 control-label">Alamat</label>

                                <div class="col-md-6">
                                    <input id="alamat2" type="alamat2" class="form-control" name="alamat2" placeholder="alamat2">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="kode_pos" class="col-md-4 control-label">Kode Pos</label>

                                <div class="col-md-6">
                                    <input id="kode_pos" type="kode_pos" class="form-control" name="kode_pos" placeholder="kode_pos">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="kota" class="col-md-4 control-label">Kota</label>

                                <div class="col-md-6">
                                    <input id="kota" type="kota" class="form-control" name="kota" placeholder="kota">
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="provinsi" class="col-md-4 control-label">Provinsi</label>

                                <div class="col-md-6">
                                    <input id="provinsi" type="provinsi" class="form-control" name="provinsi" placeholder="provinsi">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="gereja" class="col-md-4 control-label">Gereja</label>

                                <div class="col-md-6">
                                    <input id="gereja" type="gereja" class="form-control" name="gereja" placeholder="gereja">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="instagram" class="col-md-4 control-label">Instagram</label>

                                <div class="col-md-6">
                                    <input id="instagram" type="instagram" class="form-control" name="instagram" placeholder="instagram">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection