@extends('layouts.app')

@section('content')

  <script type=javascript>
  $(function() {
    $("#datepicker").datepicker();
  });
  </script>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Tambah Siswa</div>
				<div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('siswa/save') }}">
                        {{ csrf_field() }}
							
							<div class="form-group">
							<label for="name" class="col-md-4 control-label">Nama</label>
                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" placeholder=''>
                            </div>
							</div>
							

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">No. Induk Siswa Nasional</label>
                            <div class="col-md-6">
                                <input id="nisn" type="text" class="form-control" name="nisn" placeholder=''>
                            </div>
                            </div>
							
							
							<div class="form-group">
							<label for="name" class="col-md-4 control-label">Jenis Kelamin</label>
                            <div class="col-md-6">
                                <!--<input id="nama" type="text" class="form-control" name="jenis_kel" placeholder='masukkan jenis kelamin'>-->
								<select name="jenis_kel" class="form-control">
									<option value="laki_laki">Laki-Laki</option>
									<option value="perempuan">Perempuan</option>
                                </select>
                            </div>
							</div>


                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tanggal Lahir</label>
                            <div class="col-md-6">
                                <input id="tgl_lahir" type="date" class="form-control" name="tgl_lahir" placeholder=''>
                            </div>
                            </div>
							
							
							<div class="form-group">
							<label for="name" class="col-md-4 control-label">Alamat</label>
                            <div class="col-md-6">
                                <input id="tempat_lahir" type="text" class="form-control" name="tempat_lahir" placeholder=''>
                            </div>
							</div>
						
							
							<div class="form-group">
							<label for="name" class="col-md-4 control-label">Agama</label>
                            <div class="col-md-6">
                                <input id="agama" type="text" class="form-control" name="agama" placeholder=''>
                            </div>
							</div>


                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Sekolah Asal</label>
                            <div class="col-md-6">
                                <input id="sekolah_asal" type="text" class="form-control" name="sekolah_asal" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">No Ijazah</label>
                            <div class="col-md-6">
                                <input id="no_ijazah" type="text" class="form-control" name="no_ijazah" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tahun Ijazah</label>
                            <div class="col-md-6">
                                <input id="tahun_ijazah" type="text" class="form-control" name="tahun_ijazah" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">No SKHUN</label>
                            <div class="col-md-6">
                                <input id="no_skhun" type="text" class="form-control" name="no_skhun" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tahun SKHUN</label>
                            <div class="col-md-6">
                                <input id="tahun_skhun" type="text" class="form-control" name="tahun_skhun" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tanggal Masuk</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="diterima_tgl" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Nama Ayah</label>
                            <div class="col-md-6">
                                <input id="nama_ayah" type="text" class="form-control" name="nama_ayah" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Nama Ibu</label>
                            <div class="col-md-6">
                                <input id="nama_ibu" type="text" class="form-control" name="nama_ibu" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Alamat Orut</label>
                            <div class="col-md-6">
                                <input id="alamat_ortu" type="text" class="form-control" name="alamat_ortu" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Telepon Ortu</label>
                            <div class="col-md-6">
                                <input id="telepon_otu" type="text" class="form-control" name="telepon_ortu" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Alamat Siswa</label>
                            <div class="col-md-6">
                                <input id="alamat_siswa" type="text" class="form-control" name="alamat_siswa" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Telepon Siswa</label>
                            <div class="col-md-6">
                                <input id="telepon_siswa" type="text" class="form-control" name="telepon_siswa" placeholder=''>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Kelas</label>
                            <div class="col-md-6">
                            <select name="kelas_id" class="form-control">
                            @foreach($kelas as $row)
                                    <option value="{{$row['id_kelas']}}">{{ $row['nama_kelas'] }}</option>
                            @endforeach
                            </select>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Foto</label>
                            <div class="col-md-6">
                                <input id="foto" type="text" class="form-control" name="foto" placeholder=''>
                            </div>
                            </div>
							

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                     save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
				
            </div>
        </div>
    </div>
</div>

@endsection
