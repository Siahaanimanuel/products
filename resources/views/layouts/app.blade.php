<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Product Management</title>
    <link rel="shortcut icon" href="{{url('assets/logo.png')}}">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">


    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />        
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

        .cuadro_intro_hover{
        padding: 0px;
        position: relative;
        overflow: hidden;
        height: 200px;
    }
    .cuadro_intro_hover:hover .caption{
        opacity: 1;
        transform: translateY(-150px);
        -webkit-transform:translateY(-150px);
        -moz-transform:translateY(-150px);
        -ms-transform:translateY(-150px);
        -o-transform:translateY(-150px);
    }
    .cuadro_intro_hover img{
        z-index: 4;
    }
    .cuadro_intro_hover .caption{
        position: absolute;
        top:150px;
        -webkit-transition:all 0.3s ease-in-out;
        -moz-transition:all 0.3s ease-in-out;
        -o-transition:all 0.3s ease-in-out;
        -ms-transition:all 0.3s ease-in-out;
        transition:all 0.3s ease-in-out;
        width: 100%;
    }
    .cuadro_intro_hover .blur{
        background-color: rgba(0,0,0,0.7);
        height: 300px;
        z-index: 5;
        position: absolute;
        width: 100%;
    }
    .cuadro_intro_hover .caption-text{
        z-index: 10;
        color: #fff;
        position: absolute;
        height: 300px;
        text-align: center;
        top:-20px;
        width: 100%;
    }

      .navbar-inverse{
        background-color:  #366d8c;
        color: #ffffff;
       }
    </style>



</head>
<body id="app-layout">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <font size="4"><img  width="30" height="30" src="{{url('assets/logo.png')}}">Product Management</font> <BR>
					<font size="1.5"></font> 
                </a>
				</div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @if (!Auth::guest() AND Auth::user()->user_type == 'admin')
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Data Pengajuan</a></li>
                    <li><a href="{{ url('/pengajuan/create') }}">Buat Pengajuan</a></li>
                </ul>
                @endif


                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
					<li><a href="{{ url('/beranda') }}">Beranda</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')


    <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                <div class="col-lg-3 col-md-6">
                <div class="container">
            <br/>
                <hr>
                    <p>Copyright © 2019 - PRODUK MANAGEMENT | <a href="">by. Imanuel </a> | All Right Reserved
        </footer>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>
