<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Product Management</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--
Template 2033 Business
http://www.tooplate.com/view/2033-business
-->
<link href="assets/tooplate_style.css" rel="stylesheet" type="text/css" />
 <link rel="shortcut icon" href="assets/logopmk.jpg">
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Product Management</title>
    <link rel="shortcut icon" href="{{url('assets/logopmk.jpg')}}">

    

</head>
<body> 

<script type="text/javascript">
        var image1 = new Image()
            image1.src = "assets/banneraja.jpg"
        var image2 = new Image()
            image2.src = "assets/banneraja2.jpg"
        var image3 = new Image()
            image3.src = "assets/banneraja3.jpg"
        </script>

<div id="tooplate_header_wrapper">

    <div id="tooplate_header">
    
        <div id="site_title">
        
            <h3><a href="#" style="text-decoration:none">
            <img src="assets/logolagi.jpg"/>
            <span><br><h5>Perhimpunan Mahasiswa Katolik<br>Republik Indonesia</h5></span></a></h3>
        
        </div> <!-- end of site_title -->
        
        <div id="header_phone_no">

            <img src="assets/home.png"/>&nbsp;Jl. Dr. GSSJ Ratulangi No.1, RT.2/RW.3, Gondangdia, Kec. Menteng, <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10350 <br>
        
        </div>
        
        <div class="cleaner_h10"></div>
        
        <div id="tooplate_menu">
            
            <div id="home_menu"><a href="#"></a></div>
                
            <ul>
                 @if (!Auth::guest() AND Auth::user()->user_type == 'admin')
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Beranda</a></li>
                    <li><a href="{{ url('/kelas') }}">Kelas</a></li>
                    <li><a href="{{ url('/guru') }}">Guru</a></li>
                    <li><a href="{{ url('/siswa') }}">Siswa</a></li>
                    <li><a href="{{ url('/kurikulum') }}">Kurikulum</a></li>
                    <li><a href="{{ url('/jadwal_pelajaran') }}">Jadwal</a></li>
                </ul>
                @elseif(!Auth::guest() AND Auth::user()->user_type == 'siswa')
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/nilai') }}">Nilai</a></li>
                    <li><a href="{{ url('/jadwal') }}">Jadwal</a></li>
                </ul>
                @elseif(!Auth::guest() AND Auth::user()->user_type == 'guru')
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/absen') }}">Absen</a></li>
                    <li><a href="{{ url('/nilai') }}">Isi Nilai</a></li>
                    <li><a href="{{ url('/jadwal') }}">Jadwal Mengajar</a></li>
                    <li><a href="{{ url('/nilai') }}">Bank Soal</a></li>
                </ul>
                 @elseif(!Auth::guest() AND Auth::user()->user_type == 'kurikulum')
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/nilai') }}">Penjadwalan</a></li>
                    <li><a href="{{ url('/jadwal') }}">Kalender</a></li>
                    <li><a href="{{ url('/jadwal') }}">Pengumuman</a></li>
                </ul>
                @endif

                <!-- Right Side Of Navbar -->
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                    <li><a href="{{ url('/beranda') }}">Beranda</a></li>
                    <li><a href="{{ url('/Pengajuan') }}">Pengajuan</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
            </ul>


                <!-- Left Side Of Navbar -->
    </div><!-- end of tooplate_menu -->  
</div> <!-- end of header_wrapper -->

<div id="tooplate_middle_wrapper1">
    <div id="tooplate_middle_wrapper2">
        <img src = "assets/banner0.jpg" name = "slide" width = "100%" height = "100%">
            <script type = "text/javascript">
                    <!--
                    var step = 1
                    function slideit() {
                        document.images.slide.src = eval("image"+step+".src")
                        if (step<4)
                            step++
                        else
                            step = 1
                            setTimeout("slideit()", 2500)
                    }

                    slideit()
                    //-->
                </script>

        <!--<div id="tooplate_middle">

            
            <h1>SMA PSKD<span>Melayani masyarakat Jakarta melalui pendidikan bermutu & terjangkau</span></h1>
            
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ut lorem id mauris cursus pellentesque. Donec lobortis magna at orci blandit ac lobortis ipsum vestibulum.</p>
            
            <a href="#"><span>+</span> More</a>
            
        </div> -->
    </div>
</div>

<div id="tooplate_main">
    
    <div id="tooplate_content">

         <center><h3><B>Selamat Datang Di Website PMKRI Jakarta Pusat</B></h3></center><br>

        <center><img src="assets/bannertengah.jpg" width="100%" height="100%"/></center><br><br>

        <center><p><em>PMKRI Jakarta Pusat</em></p><br><br></center>

        

        <div class="cleaner_h40"></div>
        
        
            
    </div>
    
        <div id="tooplate_sidebar">
        
            <h2>PMKRI Jakarta Pusat</h2>
            
            <ul class="tooplate_list">
            </ul>
            <div class="button"><a href="">Portal</a></div>
            
            <div class="cleaner_h60"></div>
            
            <h2>Kalender Kegiatan</h2>
        
            <p>Kegiatan Komunitas<br>
            PMKRI Jakarta Pusat</p><br>

            <center><B>JADWAL KEGIATAN</B></center><br>
             <blockquote>
                <table align='center' width="280px">
                    <tr>
                        <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Waktu</b></td>
                        <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kegiatan</b></td>
                        <tr><td>&nbsp;&nbsp;&nbsp;</td></tr>
                    </tr>
                @if(count($kalender) > 0)
                @foreach($kalender as $b)
                    <tr>
                        <td >{{$b['waktu']}}</td>
                        <td>{{$b['kegiatan']}}</td>
                    </tr>
                @endforeach
                </table>
                @else
                Tidak Ada Jadwal
             @endif <br><br>
            
            <center><cite>2018 - <span>Jadwal Kegiatan<br>
            PMKRI Jakarta Pusat</span></cite></center>
            </blockquote>     
        </div>
        
        <div class="cleaner"></div>

</div>

    <div class="cleaner"></div>  

<div id="tooplate_footer_wrapper">

     <div id="tooplate_footer">
    
        Copyright © 2017 <a href="http://pmkri.org/">PMKRI Jakarta Pusat</a> - Design: <a href="http://www.tooplate.com">tooplate</a> -  Erlin<br>
        All Right Reserved
    
    </div> <!-- end of tooplate_footer -->
</div> 
    
</body>
</html>
