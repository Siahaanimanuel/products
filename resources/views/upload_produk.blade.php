@extends('layouts.app')

@section('content')



    <div class="container">
        <div class="row">
            <form class="form-horizontal" role="form" action="{{url('produk/upload/save/'.$produk['id_produk'])}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group"><br>
                    <center><img src="{{url('assets/upload2.png')}}" alt="" class="img-rounded img-responsive" width="150" height="150/"></center><br><br>
                    <center>Selamat datang, upload Foto Anda Disini
                        <p>Max upload: 500KB .JPG</p></center><br>
                    <center><label for="name" class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="foto" type="file" class="form-control" name="foto">
                        </div>
                    </center>
                    <center><div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <br><button type="submit" class="btn btn-primary">
                                    Upload
                                </button><br><br><br><br><br><br>
                            </div>
                        </div>
                    </center>
                </div>
            </form>
        </div>
    </div>

@endsection