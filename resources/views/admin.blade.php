@extends('layouts.app')

@section('content')

<style>
   h3{
      text-align:center; }
   table { 
      border-collapse:collapse;
      border-spacing:0;     
      font-family:Arial, sans-serif;
      font-size:16px;
      padding-left:300px;
      margin:auto; }
   th {
      font-weight:bold;
      padding:10px;
      color:#fff;
      background-color:#2A72BA;
      border-top:1px black solid;
      border-bottom:1px black solid;}
   td {
      padding:10px;
      border-top:1px black solid;
      border-bottom:1px black solid;
      text-align:center; }         
   tr:nth-child(even) {
     background-color: #DFEBF8; }
  </style>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"><center><b>Data Pengurus PMKRI Jakarta Pusat</b></center></div>
				
				<br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href={{url('admin/create')}} class='btn btn-default'><b>Tambah Admin</b></a><br>

               <div class="panel-body">

					<table class="table table-responsive">
					<tr>
						<th><center>Nama</center></th>
						<th><center>Jenis Kelamin</center></th>
						<th><center>Jabatan</center></th>
						<th><center>Alaamt</center></th>
						<th><center>Telepon</center></th>
                        <th><center>pilihan</center></th>
					</tr>
					@if(count($admin) > 0)
					@foreach($admin as $b)
					<tr>
                        <td>{{$b['nama']}}</td>
						<td>{{$b['jenis_kel']}}</td>
						<td>{{$b['jabatan']}}</td>
						<td>{{$b['alamat']}}</td>
						<td>{{$b['telepon']}}</td>
						<td>
							<a href="{{url('/anggota-validate/'.$b['id_admin'])}}" class="btn btn-default btn-xs">Lihat</a>
                            <a href="{{url('/anggota/hapus/'.$b['id_admin'])}}" class="btn btn-default btn-xs">Hapus</a>
						</td>

					</tr>
					@endforeach
					@else
						<tr>
							<td>data kosong</td>
						</tr>
					@endif
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
