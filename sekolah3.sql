-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2017 at 06:08 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sekolah3`
--

-- --------------------------------------------------------

--
-- Table structure for table `absen`
--

CREATE TABLE `absen` (
  `absen_id` int(10) NOT NULL,
  `siswa_id` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absen`
--

INSERT INTO `absen` (`absen_id`, `siswa_id`, `id_kelas`, `id_mapel`, `ket`, `tgl`) VALUES
(7, 22, 2, 20, 'sakit', '2017-08-24'),
(10, 22, 2, 19, 'masuk', '2017-08-24'),
(13, 22, 2, 20, 'masuk', '2017-08-28'),
(14, 22, 2, 20, 'sakit', '2017-08-31'),
(15, 23, 2, 20, 'masuk', '2017-08-31'),
(16, 24, 2, 20, 'masuk', '2017-08-31'),
(17, 22, 2, 20, 'sakit', '2017-09-05'),
(18, 23, 2, 20, 'masuk', '2017-09-05'),
(19, 24, 2, 20, 'masuk', '2017-09-05'),
(20, 22, 2, 20, 'sakit', '2017-09-06'),
(21, 23, 2, 20, 'masuk', '2017-09-06'),
(22, 24, 2, 20, 'masuk', '2017-09-06'),
(23, 22, 2, 20, 'masuk', '2017-09-06'),
(24, 23, 2, 20, 'masuk', '2017-09-06'),
(25, 24, 2, 20, 'masuk', '2017-09-06'),
(26, 22, 2, 20, 'sakit', '2017-09-12'),
(27, 23, 2, 20, 'masuk', '2017-09-12'),
(28, 24, 2, 20, 'masuk', '2017-09-12'),
(29, 22, 2, 20, 'masuk', '2017-09-13'),
(30, 23, 2, 20, 'masuk', '2017-09-13'),
(31, 24, 2, 20, 'masuk', '2017-09-13'),
(32, 28, 2, 20, 'masuk', '2017-09-13'),
(33, 29, 2, 20, 'masuk', '2017-09-13'),
(34, 30, 2, 20, 'masuk', '2017-09-13'),
(35, 31, 2, 20, 'masuk', '2017-09-13');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(16) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jenis_kel` varchar(15) NOT NULL,
  `jabatan` varchar(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telepon` double NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `jenis_kel`, `jabatan`, `alamat`, `telepon`, `foto`) VALUES
(1, 'Erlin', 'laki-laki', 'Kepala T.U', 'Jalan Tamansari X No. 17a', 87788174637, 'admin_photo_with_id1_date_12_09_17.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `guru_id` int(10) NOT NULL,
  `nip` int(10) NOT NULL,
  `mapel_id` int(10) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `pendidikan` varchar(3) NOT NULL,
  `jenis_kel` varchar(10) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telepon` int(13) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`guru_id`, `nip`, `mapel_id`, `nama`, `pendidikan`, `jenis_kel`, `alamat`, `telepon`, `foto`) VALUES
(4, 1344574873, 23, 'Sri Rohayati', 's1', 'perempuan', 'Jalan Ampar No. 31 Jakarta Barat', 217866756, 'guru_photo_with_id4_date_12_09_17.jpg'),
(7, 1386758476, 1, 'Yudi Hendrawati', 'S1', 'laki_laki', 'Jalan Tamansari X No. 17a Jakarta Barat', 21787488, ''),
(8, 1347873872, 2, 'Yustin Elisa', 'S1', 'perempuan', 'Jalan Merdeka V No. 2 Jakarta Pusat', 21348869, NULL),
(9, 13898554, 3, 'Sartik Dewi Sri', 'S1', 'perempuan', 'Jalan Dwiwarna IV No.17 Jakarta Pusat', 21766677, ''),
(10, 1325546534, 4, 'Waldiansyah Saputra', 'S1', 'laki_laki', 'Jalan Kebun Jeruk XIII No.1 Jakarta Barat', 21777666, ''),
(11, 1389878473, 5, 'Siska Siahaan', 'S1', 'perempuan', 'Jalan Kartini VII No. 84 Jakarta Pusat', 2147483647, ''),
(12, 1396887867, 8, 'Listy Dwi Erna', 'per', 'perempuan', 'Jalan Benyamin Sueb No. 14 Jakarta Pusat', 2147483647, ''),
(13, 1311243212, 9, 'Netti Budiarti', 'per', 'perempuan', 'Jalan Matraman Dalam V No. 4 Jakarta Pusat', 21867785, ''),
(14, 1396885748, 10, 'Sabah Barus', 'lak', 'laki_laki', 'Jalan Kebon Kacang X Jakarta Pusat', 21876884, ''),
(15, 1389786785, 11, 'Risma Samosir', 'per', 'perempuan', 'Jalan Tamansari VIII No. 4 Jakarta Pusat', 21566764, ''),
(16, 1390789586, 12, 'Mangasi Situmorang', 'per', 'laki_laki', 'Jalan Pulo Gadung XI No. 3 Jakarta Timur', 21348896, ''),
(17, 1398883092, 13, 'Rifai Ferdiansyah', 'lak', 'laki_laki', 'Jalan Semangka II No. 76 Jakarta Barat', 21348997, ''),
(18, 1389770352, 14, 'Rusmina Pardede', 'per', 'perempuan', 'Jalan Karang Anyar XI No. 36 Jakarta Pusat', 21398778, ''),
(19, 1387998374, 15, 'Lim Herniati', 'per', 'perempuan', 'Jalan Kramat Raya No. 16 Jakarta Pusat', 21368857, ''),
(20, 1325489687, 16, 'Endah Sari', 'per', 'perempuan', 'Jalan Manggis V Jakarta Pusat', 217996764, ''),
(21, 1389777468, 17, 'Benan Sitinjak', 'per', 'laki_laki', 'Jalan Petojo Utara No. 22 Jakarta Pusat', 2147483647, ''),
(22, 1348867857, 18, 'Nuel Siahaan', 'per', 'laki_laki', 'Jalan Tamansari X No. 17a Jakarta Barat', 2147483647, ''),
(23, 1390567764, 19, 'Andi Nurmansyar', 'per', 'laki_laki', 'Jalan Cemara 12 No. 34 Jakarta Timur', 21798746, ''),
(24, 134886785, 20, 'Endang Dermawan', 'S1', 'laki_laki', 'Jalan Cikini VI No. 9c Jakarta Pusat', 219858864, 'guru_photo_with_id24_date_10_09_17.jpg'),
(25, 1358977584, 21, 'Imas Eliana', 'per', 'perempuan', 'Jalan Tomang Barat VI No. 18 Jakarta Pusat', 129988543, ''),
(26, 1389786764, 22, 'Samsul Subekti', 'per', 'laki_laki', 'Jalan Sarinah VI No. 12 Jakarta Pusat', 21887847, '');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_pelajaran`
--

CREATE TABLE `jadwal_pelajaran` (
  `jadwal_id` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `hari` varchar(255) NOT NULL,
  `jam` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_pelajaran`
--

INSERT INTO `jadwal_pelajaran` (`jadwal_id`, `id_kelas`, `mapel_id`, `hari`, `jam`) VALUES
(10, 2, 3, 'senin', '06:30:00'),
(11, 2, 4, 'senin', '07:15:00'),
(12, 2, 5, 'senin', '08:00:00'),
(13, 2, 5, 'senin', '08:45:00'),
(14, 2, 6, 'senin', '10:00:00'),
(15, 2, 2, 'senin', '10:45:00'),
(16, 2, 7, 'senin', '11:30:00'),
(17, 2, 8, 'senin', '12:45:00'),
(18, 2, 8, 'senin', '13:30:00'),
(19, 2, 9, 'senin', '14:15:00'),
(20, 2, 10, 'selasa', '06:30:00'),
(21, 2, 10, 'selasa', '07:15:00'),
(22, 2, 11, 'selasa', '08:00:00'),
(23, 2, 11, 'selasa', '08:45:00'),
(24, 2, 12, 'selasa', '10:00:00'),
(25, 2, 12, 'selasa', '10:45:00'),
(26, 2, 13, 'selasa', '11:30:00'),
(27, 2, 13, 'selasa', '12:45:00'),
(28, 2, 14, 'selasa', '13:30:00'),
(29, 2, 14, 'selasa', '14:15:00'),
(30, 2, 15, 'rabu', '06:30:00'),
(31, 2, 15, 'rabu', '07:15:00'),
(32, 2, 16, 'rabu', '08:00:00'),
(33, 2, 16, 'rabu', '08:45:00'),
(34, 2, 2, 'rabu', '10:00:00'),
(35, 2, 6, 'rabu', '10:45:00'),
(36, 2, 5, 'rabu', '11:30:00'),
(37, 2, 5, 'rabu', '12:45:00'),
(38, 2, 12, 'rabu', '13:30:00'),
(39, 2, 12, 'rabu', '14:15:00'),
(40, 2, 13, 'kamis', '06:30:00'),
(41, 2, 13, 'kamis', '07:15:00'),
(42, 2, 5, 'kamis', '08:00:00'),
(43, 2, 5, 'kamis', '08:45:00'),
(44, 2, 14, 'kamis', '10:00:00'),
(45, 2, 14, 'kamis', '10:45:00'),
(46, 2, 15, 'kamis', '11:30:00'),
(47, 2, 15, 'kamis', '12:45:00'),
(48, 2, 8, 'kamis', '13:30:00'),
(49, 2, 8, 'kamis', '14:15:00'),
(50, 2, 12, 'jumat', '06:30:00'),
(51, 2, 12, 'jumat', '07:15:00'),
(52, 2, 13, 'jumat', '08:00:00'),
(53, 2, 13, 'jumat', '08:45:00'),
(54, 2, 14, 'jumat', '10:00:00'),
(55, 2, 14, 'jumat', '10:45:00'),
(56, 2, 2, 'jumat', '11:30:00'),
(57, 2, 2, 'jumat', '13:30:00'),
(58, 1, 12, 'senin', '06:30:00'),
(59, 1, 12, 'senin', '07:15:00'),
(60, 1, 15, 'senin', '08:00:00'),
(61, 1, 15, 'senin', '08:45:00'),
(62, 1, 16, 'senin', '10:00:00'),
(63, 1, 16, 'senin', '10:45:00'),
(64, 1, 19, 'senin', '11:30:00'),
(65, 1, 19, 'senin', '00:45:00'),
(66, 1, 18, 'senin', '13:30:00'),
(67, 1, 18, 'senin', '14:15:00'),
(68, 1, 17, 'selasa', '06:30:00'),
(69, 1, 17, 'selasa', '07:15:00'),
(70, 1, 11, 'selasa', '08:00:00'),
(71, 1, 11, 'selasa', '08:45:00'),
(72, 1, 10, 'selasa', '10:00:00'),
(73, 1, 10, 'selasa', '10:45:00'),
(74, 1, 9, 'selasa', '11:30:00'),
(75, 1, 9, 'selasa', '12:45:00'),
(76, 1, 8, 'selasa', '13:30:00'),
(77, 1, 8, 'selasa', '14:15:00'),
(78, 1, 7, 'rabu', '06:30:00'),
(79, 1, 7, 'rabu', '07:15:00'),
(80, 1, 1, 'rabu', '08:00:00'),
(81, 1, 6, 'rabu', '08:45:00'),
(82, 1, 5, 'rabu', '10:00:00'),
(83, 1, 5, 'rabu', '10:45:00'),
(84, 1, 4, 'rabu', '11:30:00'),
(85, 1, 4, 'rabu', '12:45:00'),
(86, 1, 3, 'rabu', '13:30:00'),
(87, 1, 3, 'rabu', '14:15:00'),
(88, 1, 19, 'kamis', '06:30:00'),
(89, 1, 19, 'kamis', '07:15:00'),
(90, 1, 17, 'kamis', '08:00:00'),
(91, 1, 17, 'kamis', '08:45:00'),
(92, 1, 16, 'kamis', '10:00:00'),
(93, 1, 16, 'kamis', '10:45:00'),
(94, 1, 2, 'kamis', '11:30:00'),
(95, 1, 6, 'kamis', '12:45:00'),
(96, 1, 5, 'kamis', '01:30:00'),
(97, 1, 5, 'kamis', '14:15:00'),
(98, 1, 16, 'jumat', '06:30:00'),
(99, 1, 16, 'jumat', '07:15:00'),
(100, 1, 8, 'jumat', '08:00:00'),
(101, 1, 8, 'jumat', '08:45:00'),
(102, 1, 18, 'jumat', '10:00:00'),
(103, 1, 18, 'jumat', '10:45:00'),
(104, 1, 17, 'jumat', '11:30:00'),
(105, 1, 16, 'jumat', '13:30:00'),
(106, 4, 23, 'senin', '06:30:00'),
(107, 4, 23, 'senin', '07:15:00'),
(108, 4, 21, 'senin', '08:00:00'),
(109, 4, 21, 'senin', '08:45:00'),
(110, 4, 15, 'senin', '10:00:00'),
(111, 4, 15, 'senin', '10:45:00'),
(112, 4, 20, 'senin', '11:30:00'),
(113, 4, 20, 'senin', '12:45:00'),
(114, 4, 10, 'senin', '13:30:00'),
(115, 4, 10, 'senin', '14:15:00'),
(116, 4, 9, 'selasa', '06:30:00'),
(117, 4, 9, 'selasa', '07:15:00'),
(118, 4, 18, 'selasa', '08:00:00'),
(119, 4, 18, 'selasa', '08:45:00'),
(120, 4, 14, 'selasa', '10:00:00'),
(121, 4, 14, 'selasa', '10:45:00'),
(122, 4, 12, 'selasa', '11:30:00'),
(123, 4, 12, 'selasa', '12:45:00'),
(124, 4, 13, 'selasa', '13:30:00'),
(125, 4, 13, 'selasa', '14:15:00'),
(126, 4, 6, 'rabu', '06:30:00'),
(127, 4, 6, 'rabu', '07:15:00'),
(128, 4, 8, 'rabu', '08:00:00'),
(129, 4, 8, 'rabu', '08:45:00'),
(130, 4, 5, 'rabu', '10:00:00'),
(131, 4, 5, 'rabu', '10:45:00'),
(132, 4, 4, 'rabu', '11:30:00'),
(133, 4, 4, 'rabu', '12:45:00'),
(134, 4, 3, 'rabu', '13:30:00'),
(135, 4, 3, 'rabu', '14:15:00'),
(136, 4, 6, 'kamis', '06:30:00'),
(137, 4, 6, 'kamis', '07:15:00'),
(138, 4, 5, 'kamis', '08:00:00'),
(139, 4, 5, 'kamis', '08:45:00'),
(140, 4, 8, 'kamis', '10:00:00'),
(141, 4, 8, 'kamis', '10:45:00'),
(142, 4, 13, 'kamis', '11:30:00'),
(143, 4, 13, 'kamis', '12:45:00'),
(144, 4, 12, 'kamis', '13:30:00'),
(145, 2, 12, 'kamis', '14:15:00'),
(146, 5, 1, 'senin', '06:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `kalender`
--

CREATE TABLE `kalender` (
  `id_kalender` int(16) NOT NULL,
  `waktu` varchar(255) NOT NULL,
  `kegiatan` varchar(255) NOT NULL,
  `tgl_buat` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kalender`
--

INSERT INTO `kalender` (`id_kalender`, `waktu`, `kegiatan`, `tgl_buat`) VALUES
(2, '1 - 11 Juli', 'Libur semester', '0011-11-11'),
(3, '11 Juli', 'Hari Pertama Masuk', '2017-08-26'),
(4, '17 Agt', 'Libur Nasional', '2017-08-26'),
(5, '1 Sep', 'Libur Nasional', '2017-08-26'),
(6, '21 Sep', 'Libur Nasional', '0000-00-00'),
(7, '1 Des', 'Libur Nasional', '2017-08-26'),
(8, '11 - 15 Des', 'Ulangan Umum', '2017-08-26'),
(9, '22 Des', 'Penerimaan Raport', '2017-08-26'),
(10, '25 Des', 'Libur Nasional', '2017-08-26'),
(11, '26 Des - 6 Jan', 'Libur Semester', '2017-08-26'),
(12, '26 Des', 'Libur Nasional', '2017-08-26'),
(13, '8 Jan', 'Hari Pertama Masuk', '2017-08-26'),
(14, '1 Jan', 'Libur Nasional', '2017-08-26'),
(15, '16 Feb', 'Libur Nasional', '2017-08-26'),
(16, '18 Mar', 'Libur Nasional', '2017-08-26'),
(17, '30 Mar', 'Libur Nasional', '2017-08-26'),
(18, '13 Apr', 'Libur Nasional', '2017-08-26'),
(19, '1 Mei', 'Libur Nasional', '2017-08-26'),
(20, '10 Mei', 'Libur Nasional', '2017-08-26'),
(21, '16 - 18 Mei', 'Libur Ramadhan', '2017-08-26'),
(22, '29 Mei', 'Libur Nasional', '2017-08-26'),
(23, '1 Juni', 'Libur Nasional', '2017-08-26'),
(24, '4 - 8 Jun', 'Libur Nasional', '2017-08-26'),
(25, '13 Jun', 'Penerimaan Raport', '2017-08-26'),
(26, '14 Jun - 7 Jul', 'Libur Semester', '0000-00-00'),
(27, '15 - 16 Jun', 'Libur Nasional', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(10) NOT NULL,
  `tingkatan` varchar(3) NOT NULL,
  `nama_kelas` varchar(255) NOT NULL,
  `kapasitas` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `tingkatan`, `nama_kelas`, `kapasitas`) VALUES
(1, 'X', 'X-IPS', 30),
(2, 'X', 'X-MIPA', 30),
(3, 'XI', 'XI-IPS', 30),
(4, 'XI', 'XI-MIPA', 30),
(5, 'XII', 'XII-IPS', 30),
(6, 'XII', 'XII-MIPA', 35);

-- --------------------------------------------------------

--
-- Table structure for table `kepsek`
--

CREATE TABLE `kepsek` (
  `id_kepsek` int(16) NOT NULL,
  `nama_kepala` varchar(255) NOT NULL,
  `nip` int(16) NOT NULL,
  `tahun_jabatan` varchar(20) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kepsek`
--

INSERT INTO `kepsek` (`id_kepsek`, `nama_kepala`, `nip`, `tahun_jabatan`, `jenis_kelamin`, `telepon`, `alamat`, `foto`) VALUES
(1, 'Ari Kristianti', 1356766738, '2013-2018', 'perempuan', '02178778475', 'Jalan Angkasa II, Jakarta Pusat', 'kepsek_photo_with_id1_date_12_09_17.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `keuangan`
--

CREATE TABLE `keuangan` (
  `id_keuangan` int(16) NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  `nama_depan` varchar(20) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keuangan`
--

INSERT INTO `keuangan` (`id_keuangan`, `nama_pegawai`, `nama_depan`, `jabatan`, `alamat`, `foto`) VALUES
(4, 'Aldiansyah Putra', 'aldiansyah', 'Staff 2', 'Jalan Tamansari X No. 17a Jakarta Barat', 'keuangan_photo_with_id4_date_12_09_17.jpg'),
(5, 'Erlin', 'Imanuel', 'Pegawai Utama', 'Tamansari', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keuangansiswa`
--

CREATE TABLE `keuangansiswa` (
  `id_bayaran` int(16) NOT NULL,
  `siswa_id` int(16) NOT NULL,
  `id_kelas` int(16) DEFAULT NULL,
  `bulan` varchar(10) NOT NULL,
  `ekskul` double NOT NULL,
  `tgl_ekskul` date NOT NULL,
  `iuran_sekolah` double NOT NULL,
  `tgl_iuran` date NOT NULL,
  `id_keuangan` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `keuangansiswa`
--

INSERT INTO `keuangansiswa` (`id_bayaran`, `siswa_id`, `id_kelas`, `bulan`, `ekskul`, `tgl_ekskul`, `iuran_sekolah`, `tgl_iuran`, `id_keuangan`) VALUES
(13, 22, 2, 'Januari', 150000, '2017-09-06', 250000, '2017-09-06', 4),
(14, 23, NULL, 'Februari', 150000, '2017-09-06', 250000, '2017-09-06', 4),
(15, 22, 2, 'Februari', 150000, '2017-09-06', 250000, '2017-09-06', 4),
(16, 23, NULL, 'Februari', 150000, '2017-10-04', 250000, '2017-10-04', 4),
(17, 22, NULL, 'Maret', 150000, '2017-02-12', 250000, '2017-02-12', 4);

-- --------------------------------------------------------

--
-- Table structure for table `kurikulum`
--

CREATE TABLE `kurikulum` (
  `id_kurikulum` int(16) NOT NULL,
  `guru_id` int(16) NOT NULL,
  `nip` int(16) DEFAULT NULL,
  `jabatan` varchar(255) NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurikulum`
--

INSERT INTO `kurikulum` (`id_kurikulum`, `guru_id`, `nip`, `jabatan`, `foto`) VALUES
(13, 4, 1344574873, 'Staff', NULL),
(17, 9, 13898554, 'Staff 4', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `mapel_id` int(10) NOT NULL,
  `guru_id` int(10) NOT NULL,
  `mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`mapel_id`, `guru_id`, `mapel`) VALUES
(1, 1, 'KWN'),
(2, 8, 'Matematika'),
(3, 9, 'Pendidikan Agama dan Budi Pekerti'),
(4, 10, 'Pendidikan Pancasila dan Kewarganegaraan'),
(5, 11, 'Bahasa Indonesia'),
(6, 0, 'Matematika'),
(7, 0, 'Sejarah Indonesia'),
(8, 12, 'Bahasa Inggris'),
(9, 13, 'Seni Budaya'),
(10, 14, 'Penjaskes'),
(11, 15, 'Prakarya dan Kewirausahaan'),
(12, 16, 'Biologi'),
(13, 17, 'Fisika'),
(14, 18, 'Kimia'),
(15, 19, 'Bahasa Mandarin'),
(16, 20, 'Ekonomi'),
(17, 21, 'Geografi'),
(18, 22, 'Sejarah'),
(19, 23, 'Sosiologi'),
(20, 24, 'Teknologi Informasi dan Komunikasi'),
(21, 25, 'Muatan Lokal (English Conversation)'),
(22, 26, 'Kewirausahaan'),
(23, 4, 'BP');

-- --------------------------------------------------------

--
-- Table structure for table `mapel_map`
--

CREATE TABLE `mapel_map` (
  `id_map` int(16) NOT NULL,
  `siswa_id` int(16) NOT NULL,
  `mapel_id` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel_map`
--

INSERT INTO `mapel_map` (`id_map`, `siswa_id`, `mapel_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 1, 2),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `nilai_id` int(16) NOT NULL,
  `siswa_id` int(16) NOT NULL,
  `mapel_id` int(16) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `kkm` double NOT NULL,
  `tugas1` double NOT NULL,
  `tugas2` double NOT NULL,
  `tugas3` double NOT NULL,
  `tugas4` double NOT NULL,
  `tugas5` double NOT NULL,
  `uts` double NOT NULL,
  `uas` double NOT NULL,
  `keterampilan` double NOT NULL,
  `nilai_akhir` double NOT NULL,
  `semester` varchar(10) NOT NULL,
  `created_at` datetime(6) DEFAULT NULL,
  `update_at` datetime(6) DEFAULT NULL,
  `akhlak` varchar(15) NOT NULL,
  `kerajinan` varchar(255) NOT NULL,
  `kesopanan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`nilai_id`, `siswa_id`, `mapel_id`, `id_kelas`, `kkm`, `tugas1`, `tugas2`, `tugas3`, `tugas4`, `tugas5`, `uts`, `uas`, `keterampilan`, `nilai_akhir`, `semester`, `created_at`, `update_at`, `akhlak`, `kerajinan`, `kesopanan`) VALUES
(1, 22, 1, 2, 75, 70, 72, 65, 75, 78, 68, 80, 70, 73.6, '2017', '2017-09-02 00:00:00.000000', '2017-09-02 00:00:00.000000', 'B', 'A', 'A'),
(2, 22, 18, 2, 68, 70, 70, 80, 90, 90, 100, 90, 100, 87, '2017', '2017-09-02 00:00:00.000000', '2017-09-02 00:00:00.000000', 'A', 'A', 'A'),
(3, 22, 7, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganjil', '2017-09-04 00:00:00.000000', '2017-09-04 00:00:00.000000', 'B', 'A', 'A'),
(4, 23, 1, 2, 60, 70, 70, 70, 70, 70, 70, 70, 70, 70, '2017', '2017-09-04 00:00:00.000000', '2017-09-04 00:00:00.000000', 'B', 'a', 'a'),
(5, 23, 1, 2, 70, 70, 70, 50, 75, 70, 70, 70, 70, 68.5, 'Ganjil', '2017-09-12 00:00:00.000000', '2017-09-12 00:00:00.000000', 'Amat Baik', 'Amat Baik', 'Amat Baik'),
(6, 22, 8, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(7, 22, 8, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(8, 22, 9, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(9, 22, 10, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(10, 22, 11, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(11, 22, 12, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(12, 22, 13, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(13, 22, 14, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(14, 22, 15, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(15, 22, 16, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(16, 22, 17, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(17, 22, 8, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(18, 22, 9, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(19, 22, 10, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(20, 22, 11, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(21, 22, 12, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(22, 22, 13, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(23, 22, 14, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(24, 22, 15, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(25, 22, 16, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(26, 22, 17, 2, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(27, 22, 8, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(28, 22, 9, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(29, 22, 10, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(30, 22, 11, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(31, 22, 12, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(32, 22, 13, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(33, 22, 14, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(34, 22, 15, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(35, 22, 16, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(36, 22, 17, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(37, 22, 8, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(38, 22, 9, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(39, 22, 10, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(40, 22, 11, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(41, 22, 12, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(42, 22, 13, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(43, 22, 14, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(44, 22, 15, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(45, 22, 16, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(46, 22, 17, 4, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(47, 22, 8, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(48, 22, 9, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(49, 22, 10, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(50, 22, 11, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(51, 22, 12, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(52, 22, 13, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(53, 22, 14, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(54, 22, 15, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(55, 22, 16, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(56, 22, 17, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Ganji', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(57, 22, 8, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(58, 22, 9, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(59, 22, 10, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(60, 22, 11, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(61, 22, 12, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(62, 22, 13, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(63, 22, 14, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(64, 22, 15, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'GGenap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(65, 22, 16, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(66, 22, 17, 6, 70, 70, 70, 70, 75, 78, 70, 70, 70, 71.3, 'Genap', '2017-05-27 10:19:10.000000', '2017-05-27 10:19:10.000000', 'Baik', 'Baik', 'Baik'),
(67, 22, 20, 2, 22, 50, 50, 50, 50, 50, 50, 50, 50, 50, 'Ganji', '2017-09-13 00:00:00.000000', '2017-09-13 00:00:00.000000', 'Amat Baik', 'Amat Baik', 'Amat Baik'),
(68, 22, 20, 2, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70, 'Ganjil', '2017-09-13 00:00:00.000000', '2017-09-13 00:00:00.000000', 'Amat Baik', 'Amat Baik', 'Amat Baik');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', 'c42d6108a93b289ce0e61dc28a03bf2e37f16690a086f7af08fd215d60832505', '2017-09-11 21:47:44');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(16) NOT NULL,
  `isi` text NOT NULL,
  `tanggal_buat` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `isi`, `tanggal_buat`) VALUES
(2, 'aSehubungan dengan akan tibanya hari Kemerdekaan Republik Indonesia tanggal 17 Agustus 2017, maka pada hari rabu, 16 Agustus 2017 akan diadakan kegiatan Perayaan HUT Kemerdekaan RI ke-72 Tahun di sekolah. Perayaan berupa perlombaan (akademik dan nonakademik), menyanykan lagu kemerdekaan RI, dan kegiatan gatering. Diharapkan kepada para siswa untuk membawa perlengkapan berupa baju ganti (Kaos). Kegiatan dilaksanakan menggunakan pakaian bebas.', '2017-08-01'),
(3, 'Pada tanggal 17 Agustus 2017, seluruh siswa SMA 2 PSKD Jakarta kelas X, XI dan XI diwajibkan datang ke sekolah untuk mengikuti kegiatan Upacara Bendera memperingati hari Kemerdekaan RI ke 72 Tahun. Masuk Pukul 7.00 WIB, memakai pakaian seragam putih abu-abu lengkap dengan topi dan dasi, serta memakai kaos kaki putih dan sepatu hitam. Absen siswa tetap dilakukan.', '2017-08-01'),
(4, 'Bagi siswa yang belum melunasi iuran sekolah dan ekstra kulikuler sekolah bulan Juli 2017, diharapkan segera mlunasi kewajiban tersebut sebelum tanggal 5 Agustus 2017. Sesuai dengan keputusan didalam buku tata tertib sekolah, bagi yang belum melunasi sebelum tanggal 5 Agustus, akan diberi surat pengingat kepada pihak orang tua. Terima kasih.', '2017-08-02');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `siswa_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nisn` int(16) NOT NULL,
  `jenis_kel` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `sekolah_asal` varchar(255) NOT NULL,
  `no_ijazah` varchar(255) NOT NULL,
  `tahun_ijazah` int(4) NOT NULL,
  `no_skhun` varchar(255) NOT NULL,
  `tahun_skhun` int(4) NOT NULL,
  `diterima_tgl` date NOT NULL,
  `nama_ayah` varchar(255) NOT NULL,
  `nama_ibu` varchar(255) NOT NULL,
  `alamat_ortu` varchar(255) NOT NULL,
  `telepon_ortu` double NOT NULL,
  `alamat_siswa` varchar(255) NOT NULL,
  `telepon_siswa` double NOT NULL,
  `foto` varchar(255) NOT NULL,
  `created_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `kelas_id` int(16) DEFAULT NULL,
  `nis` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`siswa_id`, `nama`, `nisn`, `jenis_kel`, `tgl_lahir`, `tempat_lahir`, `agama`, `sekolah_asal`, `no_ijazah`, `tahun_ijazah`, `no_skhun`, `tahun_skhun`, `diterima_tgl`, `nama_ayah`, `nama_ibu`, `alamat_ortu`, `telepon_ortu`, `alamat_siswa`, `telepon_siswa`, `foto`, `created_by`, `update_by`, `kelas_id`, `nis`) VALUES
(22, 'Kinarti Riana Putri', 7765348, 'perempuan', '1999-12-25', 'Jalan Salemba VII No. 36 Jakarta Pusat', 'Kristen', 'SMP Negeri 118 Jakarta', 'MI 7688253027', 2016, 'LS 996723', 2016, '0000-00-00', 'Dedi Hermawan', 'yuni Herawati', 'Jalan Salemba VII No. 36 Jakarta Pusat', 81288750123, 'Jalan Salemba VII No. 36 Jakarta Pusat', 89995461298, 'siswa_photo_with_id22_date_10_09_17.jpg', '2017-09-10 17:32:15', '0000-00-00 00:00:00', 2, 201730022),
(23, 'Mega Herawati', 988946312, 'perempuan', '0000-00-00', 'Jalan Matraman Dalam No. 37a Jakarta Timur', 'islam', 'SMP Negeri 67 Jakarta', 'MI 0778635625', 2016, 'SM 867832', 2016, '0000-00-00', 'Yono Mulyadi', 'Lisniawati ', 'Jalan Matraman Dalam No. 37a Jakarta Timur', 87764530124, 'Jalan Matraman Dalam No. 37a Jakarta Timur', 87865773452, '', '2017-08-11 07:43:53', '0000-00-00 00:00:00', 2, 201730023),
(24, 'Henra Alexander', 76885622, 'laki_laki', '0000-00-00', 'Jalan Pramuka Raya No. 33 Jakarta Timur', 'Khatolik', 'SMP Strada II Jakarta', 'MI 078875641234', 2016, 'LS 9963244', 2016, '0000-00-00', 'Ferdi Hasan', 'Yenny Lisdawati', 'Jalan Pramuka Raya No. 33 Jakarta Timur', 21667545, 'Jalan Pramuka Raya No. 33 Jakarta Timur', 85600781353, '', '2017-08-11 07:46:14', '0000-00-00 00:00:00', 2, 201730024),
(25, 'Daniel Manurung', 800986756, 'laki_laki', '0000-00-00', 'Jalan Utan Kayu No. 32 Jakarta Timur', 'Kristen', 'SMP Negeri 113 Jakarta', 'MI 6897884623', 2016, 'SL 867453', 2016, '0000-00-00', 'Lamsihar Manurung', 'Romita Sirait', 'Jalan Utan Kayu No. 32 Jakarta Timur', 87756451234, 'Jalan Utan Kayu No. 32 Jakarta Timur', 81247563455, '', '2017-09-13 01:54:50', '0000-00-00 00:00:00', 1, 201730025),
(26, 'Gilang Ramadhan', 86775846, 'laki_laki', '0000-00-00', 'Jalan Indah No. 22 Jakarta Pusat', 'Islam', 'SMP Negeri 64 Jakarta', 'MI 0889674332', 2015, 'LM 228832', 2015, '0000-00-00', 'Mandala Putra', 'Ayu Mardini', 'Jalan Utan Kayu No. 32 Jakarta Timur', 89953645531, 'Jalan Utan Kayu No. 32 Jakarta Timur', 87767451234, '', '2017-09-13 01:54:57', '0000-00-00 00:00:00', 1, 201730026),
(27, 'Ananda Putri Elisa', 8356645, 'laki_laki', '0000-00-00', 'Jalan Matraman Dalam No. 47 Jakarta Timur', 'Kristen', 'SMP Bina Bangsa', 'MI 0889635664', 2015, 'LS 991298', 2015, '0000-00-00', 'Effendy Lim', 'Elina Tan', 'Jalan Matraman Dalam No. 47 Jakarta Timur', 88856745962, 'Jalan Matraman Dalam No. 47 Jakarta Timur', 87765741208, '', '2017-09-13 01:55:04', '0000-00-00 00:00:00', 1, 201730027),
(28, 'Vera Anissa', 8786455, 'perempuan', '0000-00-00', 'Jalan B Karang Anyar No. 8 Jakarta Pusat', 'Kristen', 'SMP Negeri 17 Jakarta', 'MI 6675835120', 2015, 'SL 556473', 2015, '0000-00-00', 'Andi Salim', 'Meilina Lim', 'Jalan B Karang Anyar No. 8 Jakarta Pusat', 87765745523, 'Jalan B Karang Anyar No. 8 Jakarta Pusat', 81233648567, '', '2017-09-13 01:55:12', '0000-00-00 00:00:00', 2, 201730028),
(29, 'Merianda Gustina', 86572453, 'perempuan', '0000-00-00', 'Jalan Kramat VI No.21 Jakarta Pusat', 'Kristen', 'SMPK Penabur Jakarta', 'MI 7855364512', 2015, 'SL 078846', 2015, '0000-00-00', 'Beni Hardidi', 'Heni Yuliani', 'Jalan Kramat VI No.21 Jakarta Pusat', 21675645, 'Jalan Kramat VI No.21 Jakarta Pusat', 81244657342, '', '2017-09-13 01:55:16', '0000-00-00 00:00:00', 2, 201730029),
(30, 'Agus Salim Tan', 6755344, 'laki_laki', '0000-00-00', 'Jalan Cempaka Dalam No. 22 Jakarta Pusat', 'islam', 'SMP Negeri 13 Jakarta', 'MI 7768550002', 2015, 'SL 657433', 2015, '0000-00-00', 'Hanin Tan', 'Lili Marina', 'Jalan Cempaka Dalam No. 22 Jakarta Pusat', 21665432, 'Jalan Cempaka Dalam No. 22 Jakarta Pusat', 89655640897, '', '2017-09-13 01:55:19', '0000-00-00 00:00:00', 2, 201730030),
(31, 'Rizha Maria', 6758253, 'perempuan', '0000-00-00', 'Jalan Cikini Raya No. 42 Jakarta Pusat', 'Khatolik', 'SMP 17 Agustus Jakarta', 'MD 9978574837', 2016, 'SF 567746', 2016, '0000-00-00', 'Hari Gunardi', 'Melisa Nilas', 'Jalan Cikini Raya No. 42 Jakarta Pusat', 21657739, 'Jalan Cikini Raya No. 42 Jakarta Pusat', 89766451208, '', '2017-09-13 01:55:24', '0000-00-00 00:00:00', 2, 201730031),
(32, 'Shintia Maria', 764670089, 'perempuan', '0000-00-00', 'Jalan Tambak No. 02 Jakarta Pusat', 'Khatolik', 'SMP Harapan Bangsa', 'MD 7768576433', 2016, 'SF 445073', 2016, '0000-00-00', 'Bachtiar R', 'Wenda Maria', 'Jalan Tambak No. 02 Jakarta Pusat', 21, 'Jalan Tambak No. 02 Jakarta Pusat', 85277680089, '', '2017-09-13 01:55:29', '0000-00-00 00:00:00', 3, 201730032),
(33, 'Deni Hendrawan', 889956473, 'laki_laki', '0000-00-00', 'Jalan Cipinang Muara No. 12 Jakarta Timur', 'Kristen', 'SMP Negeri 7 Jakarta', 'MD 7768577382', 2016, 'SF 465774', 2016, '0000-00-00', 'Dirja Hendrawan', 'Martiani Linda', 'Jalan Cipinang Muara No. 12 Jakarta Timur', 21657768, 'Jalan Cipinang Muara No. 12 Jakarta Timur', 81355098865, '', '2017-09-13 01:55:32', '0000-00-00 00:00:00', 3, 201730033),
(34, 'Yeremia SAputra', 888967564, 'laki_laki', '0000-00-00', 'Jalan Cipinang Lontar II No. 5 Jakarta Timur', 'Kristen', 'SMP Negeri 36 Jakarta', 'MD 8876857433', 2016, 'SF 560967', 2016, '0000-00-00', 'Herdi Budiono', 'Meilina Lin', 'Jalan Cipinang Lontar II No. 5 Jakarta Timur', 21670089, 'Jalan Cipinang Lontar II No. 5 Jakarta Timur', 89655460887, '', '2017-09-13 01:55:39', '0000-00-00 00:00:00', 4, 201730034),
(35, 'Edi Dirgantoro', 8776857, 'laki_laki', '0000-00-00', 'Jalan Menteng Atas Selatan III No. 22 Jakarta Selatan', 'Islam', 'SMP 35 Jakarta', 'MI 6788574627', 2016, 'SF 857784', 2016, '0000-00-00', 'Bambang Wibowo', 'Lemina Anggraini', 'Jalan Menteng Atas Selatan III No. 22 Jakarta Selatan', 21607576, 'Jalan Menteng Atas Selatan III No. 22 Jakarta Selatan', 85288793542, '', '2017-09-13 01:55:42', '0000-00-00 00:00:00', 4, 201730035),
(36, 'Ferry Rahmat', 88766573, 'laki_laki', '0000-00-00', 'Jalan Kramat VI No. 45 Jakarta Pusat', 'Islam', 'SMP Negeri 67 Jakarta', 'MI 0089786574', 2016, 'SF 578675', 2016, '0000-00-00', 'Rahmat Darus', 'Sutiani', 'Jalan Kramat VI No. 45 Jakarta Pusat', 2134665, 'Jalan Kramat VI No. 45 Jakarta Pusat', 87765708978, '', '2017-09-13 01:55:45', '0000-00-00 00:00:00', 5, 201730036),
(37, 'Iman Sanjaya', 899786956, 'laki_laki', '0000-00-00', 'Jalan Senen Dalam No. 37a Jakarta Pusat', 'Islam', 'SMP Negeri 15 Jakarta', 'MD 7786956474', 2014, 'SH 299867', 2014, '0000-00-00', 'Anton Supardi', 'Mariati', 'Jalan Senen Dalam No. 37a Jakarta Pusat', 21445641, 'Jalan Senen Dalam No. 37a Jakarta Pusat', 87758670897, '', '2017-09-13 01:55:48', '0000-00-00 00:00:00', 5, 201730037),
(38, 'Melisa Nastion', 800789567, 'perempuan', '0000-00-00', 'Jalan Pegangsaan Timur No. 12 Jakarta Pusat', 'islam', 'SMP 17 Agustus Jakarta', 'MD 0089887564', 2014, 'SH 677586', 2014, '0000-00-00', 'Dadang Supadi', 'Sinta Ningsih', 'Jalan Pegangsaan Timur No. 12 Jakarta Pusat', 21667807, 'Jalan Pegangsaan Timur No. 12 Jakarta Pusat', 87756741208, '', '2017-09-13 01:55:52', '0000-00-00 00:00:00', 6, 201730038),
(39, 'Esther Marchelina', 8878675, 'perempuan', '0000-00-00', 'Jalan Tebet Raya No. 39a Jakarta Selatan', 'Kristen', 'SMP Budaya Jakarta', 'MD 0788675847', 2014, 'SH 457768', 2014, '0000-00-00', 'Ferdinan Daud', 'Marcella Renita', 'Jalan Tebet Raya No. 39a Jakarta Selatan', 21650998, 'Jalan Tebet Raya No. 39a Jakarta Selatan', 88867584563, '', '2017-09-13 01:55:56', '0000-00-00 00:00:00', 6, 201730039);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mapel_id` int(16) NOT NULL,
  `guru_id` int(11) DEFAULT NULL,
  `siswa_id` int(11) DEFAULT NULL,
  `id_kurikulum` int(16) DEFAULT NULL,
  `id_kepsek` int(11) NOT NULL,
  `id_keuangan` int(11) NOT NULL,
  `id_admin` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `user_type`, `mapel_id`, `guru_id`, `siswa_id`, `id_kurikulum`, `id_kepsek`, `id_keuangan`, `id_admin`) VALUES
(11, 'siswa', 'siswa@gmail.com', '$2y$10$NP6euOQ65s8uiKT16aJ7MOdMxG0waaIWQVYypuW4G3hhKz8ZeULk2', 'dSj8D0BhYtkSG3002qcPnaib5FWPwZMAObgEYXo3Eu5bZ34XpTbgrUaTvf5d', '2017-05-27 03:19:10', '2017-08-21 21:37:20', 'siswa', 0, NULL, 1, 0, 0, 0, 0),
(13, 'guru', 'guru@gmail.com', '$2y$10$NP6euOQ65s8uiKT16aJ7MOdMxG0waaIWQVYypuW4G3hhKz8ZeULk2', 'N4NAsRnnjrWHyXoIabXhFwIM9xHL0x1eOiLnET1dsYoW38xoI8yaxyijUfqT', '2017-05-27 03:19:10', '2017-08-11 01:16:30', 'guru', 1, 1, NULL, 0, 0, 0, 0),
(14, 'Erlin', 'admin@gmail.com', '$2y$10$NP6euOQ65s8uiKT16aJ7MOdMxG0waaIWQVYypuW4G3hhKz8ZeULk2', 'IXmQy3v4eFHbj88unrrnEAoaL0xa3oBif3Iu3PbPAw0ZEpNtNTCjXH3IHCxW', '2017-05-27 03:19:10', '2017-09-12 21:59:52', 'admin', 0, NULL, NULL, 0, 0, 0, 1),
(17, 'wali_kelas', 'd@gmail.com', '$2y$10$NP6euOQ65s8uiKT16aJ7MOdMxG0waaIWQVYypuW4G3hhKz8ZeULk2', 'NHV1LuAGPSNRF9mF1z5gdi2ebCOSZCQzwmibjFKWd4no8GmHvtLAbzhbhcs1', '2017-05-27 03:19:10', '2017-06-04 03:07:37', 'wali_kelas', 0, NULL, NULL, 0, 0, 0, 0),
(18, 'guru', 'guru_02@gmail.com', '$2y$10$NP6euOQ65s8uiKT16aJ7MOdMxG0waaIWQVYypuW4G3hhKz8ZeULk2', 'XxTtZ8WMHFRAhIGh1cfaH6Dr4F9YwZ9vqWY0bjq5VNBgJNN5Y13jR4Jzw9pW', '2017-05-27 03:19:10', '2017-06-22 06:16:09', 'guru', 1, 2, NULL, 0, 0, 0, 0),
(19, 'aaa', 'aaa@sma2pskd.com', '$2y$10$LcP6otH4oFnshzjm/d8Cm.JmhFW4UiSzvvQ8C7OEGd32Q.qOOdcGC', 'f9m9brldl9rA76mLs7TtEZJYFCYlA5519f7Kdhg6nhXbPwlKESn46BsAsvsu', '2017-08-10 05:45:34', '2017-08-10 05:46:17', 'siswa', 0, 0, 5, 0, 0, 0, 0),
(33, 'Imanuel', '201730020@sma2pskd.com', '$2y$10$Q/MSO7SWpKQueI2OdMra9e80Ju0knHXwfJ6MvJvD0hjbK2f4NRB6y', NULL, '2017-08-10 06:49:26', '2017-08-10 06:49:26', 'siswa', 0, 0, 20, 0, 0, 0, 0),
(34, 'test', '8998346831@sma2pskd.com', '$2y$10$rC3DjaqYzCb3a0f2Td3sM.QMPm2Puh7RQlQns5lTVYRmKmazEVkQS', NULL, '2017-08-10 22:24:22', '2017-08-10 22:24:22', 'siswa', 0, 5, 0, 0, 0, 0, 0),
(35, 'Yohannes Ferdy', '201730021@sma2pskd.com', '$2y$10$RMtxjJat5iDmbHRzSQjNkOhvCFaH9J1Bi2sry4JRRD8OxmHD9Xi4.', NULL, '2017-08-11 00:38:29', '2017-08-11 00:38:29', 'siswa', 0, 0, 21, 0, 0, 0, 0),
(36, 'Kinarti Riana Putri', '201730022@sma2pskd.com', '$2y$10$reinLm1YUOG4NW3Yd5y2veDfwq5kaiS2Yq0DDPBfZ2u3tM6lcmW9q', 'rrfaFdtau4sKhQWEK2zC8YRUxOjRRXzLxr5TqTvQADISAYbC2dKfnawfINXf', '2017-08-11 00:41:27', '2017-09-12 21:57:31', 'siswa', 0, 0, 22, 0, 0, 0, 0),
(37, 'Mega Herawati', '201730023@sma2pskd.com', '$2y$10$oaKXrPFlkGmP3bQEDlm/D.Vr5YeklOMm5uDsSFj0yfpHnZwy.kmM6', '04o2Fmoj24zr21DvuFdeOHk08TXTipwTUD0Y7Z9kvWtqwX9jROqNiL0Mxce1', '2017-08-11 00:43:53', '2017-09-12 02:48:19', 'siswa', 0, 0, 23, 0, 0, 0, 0),
(38, 'Henra Alexander', '201730024@sma2pskd.com', '$2y$10$i2qIeLX4Pz3bSNxw7GMTd.ZeJeI9bfiQJFcPFwoFMolCnzD.uCGaG', 'T6Y4s5hls5r5FUvCVVVsfFuEb3ee61EBk9Rx0Zilk1gbrdUdrOLhb2zcIlfD', '2017-08-11 00:46:14', '2017-08-20 20:47:04', 'siswa', 0, 0, 24, 0, 0, 0, 0),
(39, 'Daniel Manurung', '201730025@sma2pskd.com', '$2y$10$Ppi9P16jP6zjsEiIZVTpeOMzfWuyxldgiiV7UKWgOUbvSgeMZx/w2', NULL, '2017-08-11 00:52:46', '2017-08-11 00:52:46', 'siswa', 0, 0, 25, 0, 0, 0, 0),
(40, 'Gilang Ramadhan', '201730026@sma2pskd.com', '$2y$10$YqvkEZ1LoYfZdzLAjXh.BOcWyJe38VmcSR/DRVF5ORo7qULGaVoKS', NULL, '2017-08-11 00:54:42', '2017-08-11 00:54:42', 'siswa', 0, 0, 26, 0, 0, 0, 0),
(41, 'Ananda Putri Elisa', '201730027@sma2pskd.com', '$2y$10$meWGyEd2357hlC.k0VXV9.x7.76bAsLtWhnMUFQAbJOB0BnLbTUji', NULL, '2017-08-11 00:58:13', '2017-08-11 00:58:13', 'siswa', 0, 0, 27, 0, 0, 0, 0),
(42, 'Vera Anissa', '201730028@sma2pskd.com', '$2y$10$APNvkrGTHvkVRRiTJXlUh.IsnobSGSueQEnRCTybLEUClJ0gmXoJe', NULL, '2017-08-11 01:01:03', '2017-08-11 01:01:03', 'siswa', 0, 0, 28, 0, 0, 0, 0),
(43, 'Merianda Gustina', '201730029@sma2pskd.com', '$2y$10$jGk7qWAWzuiQD6pzk1p3dO/ElWexDXpGM2NQEFvNt7mQVVp7mqWGC', NULL, '2017-08-11 01:05:36', '2017-08-11 01:05:36', 'siswa', 0, 0, 29, 0, 0, 0, 0),
(44, 'Agus Salim Tan', '201730030@sma2pskd.com', '$2y$10$qYxEIVzCUFfPJA42NSG6G.dJ0VrV/L.tb0P8rqx3Dtg4sN6uStwYO', NULL, '2017-08-11 01:07:44', '2017-08-11 01:07:44', 'siswa', 0, 0, 30, 0, 0, 0, 0),
(45, 'Rizha Maria', '201730031@sma2pskd.com', '$2y$10$pnTp.Ece1zGol8s1Rm7waOKa2Y9mWoXgP1HjjWp4pnxZE1HXF8M.G', NULL, '2017-08-11 01:10:39', '2017-08-11 01:10:39', 'siswa', 0, 0, 31, 0, 0, 0, 0),
(46, 'Shintia Maria', '201730032@sma2pskd.com', '$2y$10$aToixxQLdUQYzNcsacyYi.NWNvuC0mBxYGu226MEpr1edXVlaFVLG', NULL, '2017-08-11 01:12:55', '2017-08-11 01:12:55', 'siswa', 0, 0, 32, 0, 0, 0, 0),
(47, 'Deni Hendrawan', '201730033@sma2pskd.com', '$2y$10$WqRh5dmoFOhEc39mHenYs.0ZMYC3izBXeOiqCo1GSRgBI0GnLFSN2', NULL, '2017-08-11 01:14:53', '2017-08-11 01:14:53', 'siswa', 0, 0, 33, 0, 0, 0, 0),
(48, 'Yeremia SAputra', '201730034@sma2pskd.com', '$2y$10$xMSlUyaPvt1MajydAoviremn6gler2vjEnmuu8dp5r9VSqAEGBiEW', NULL, '2017-08-11 01:18:56', '2017-08-11 01:18:56', 'siswa', 0, 0, 34, 0, 0, 0, 0),
(49, 'Edi Dirgantoro', '201730035@sma2pskd.com', '$2y$10$kIn42GYaq7PM6Sifch9pA.NWEjdcWIvSL/6wwj4XjPNdD5g1/tQwe', NULL, '2017-08-11 01:21:56', '2017-08-11 01:21:56', 'siswa', 0, 0, 35, 0, 0, 0, 0),
(50, 'Ferry Rahmat', '201730036@sma2pskd.com', '$2y$10$u9d1vXiGQ194qbYGUgjZ7uNXVVcVnxZS6XGB6j4oLL/3.8HB/4aIi', NULL, '2017-08-11 01:24:02', '2017-08-11 01:24:02', 'siswa', 0, 0, 36, 0, 0, 0, 0),
(51, 'Iman Sanjaya', '201730037@sma2pskd.com', '$2y$10$R7rR0g5rYtsO2zYnQnRfDOKP84lam0UOO/4Cw/7Kw.hcUedxBTmrG', NULL, '2017-08-11 01:26:00', '2017-08-11 01:26:00', 'siswa', 0, 0, 37, 0, 0, 0, 0),
(52, 'Melisa Nastion', '201730038@sma2pskd.com', '$2y$10$gz6K/..gzxnpNd3ggyaaquGR/a7z9LJgwKmF87ZcuvfXLU/N0IaJK', NULL, '2017-08-11 01:27:53', '2017-08-11 01:27:53', 'siswa', 0, 0, 38, 0, 0, 0, 0),
(53, 'Esther Marchelina', '201730039@sma2pskd.com', '$2y$10$oUQd8xJRxKGaW6ImE0jutOD.ey6am/R7ujKiTSmh0W1t3108MpjT.', NULL, '2017-08-11 01:30:07', '2017-08-11 01:30:07', 'siswa', 0, 0, 39, 0, 0, 0, 0),
(54, 'Sri Rohayati', '1344574873@sma2pskd.com', '$2y$10$QEeBc0vi1l7AFIUbMNXqLeIfa9nJRJu6Ylc083DHA.IBL.CO0eRAi', 'Nk7OUYnAbdlQ3BwaAffrf6tFB3DDbJU1TQ40AU4TJTNHWIR5zhKfFt7p3K6K', '2017-08-11 06:51:31', '2017-09-11 21:31:09', 'guru', 0, 4, 0, 0, 0, 0, 0),
(55, 'Rian Nurmawan', '1377686574@sma2pskd.com', '$2y$10$6UgKrm2t37nv3Q2WTtHXXeE4XzM1JUCGiTSBe6Pg9WwmoHakbeCUu', NULL, '2017-08-11 07:15:55', '2017-08-11 07:15:55', 'siswa', 0, 5, 0, 0, 0, 0, 0),
(56, '4', '.kurikulum@sma2pskd.com', '$2y$10$ZfMZC.eMYrYZdEtNNv9MQeORHkWSfQ9P6gBho6mTk9WAkVmp3oRtW', NULL, '2017-08-11 19:33:20', '2017-08-11 19:33:20', 'kurikulum', 0, 0, 0, 0, 0, 0, 0),
(59, 'Sri Rohayati ', '1344574873.kurikulum@sma2pskd.com', '$2y$10$D1jcNKoc1FkJa/Lx4PExAOTxVGMZx.9/q5sn.WdwOBxVjDLiDCb3C', '7qFPYNFlgrXjQnU8FEhzbGe4wlzbAnR1dzfqJcBD6SFrzxDJVReWnW4GL1vB', '2017-08-11 20:30:15', '2017-09-12 09:46:47', 'kurikulum', 0, 0, 0, 13, 0, 0, 0),
(61, 'Imanuel', '26776356@sma2pskd.com', '$2y$10$R7zsAd7zlGlEx.Xx2xX.VeAa6thGwNuJMgHvp5h4RYSJzdgVsV7fu', NULL, '2017-08-11 20:34:11', '2017-08-11 20:34:11', 'siswa', 0, 6, 0, 0, 0, 0, 0),
(62, '6', '26776356.kurikulum@sma2pskd.com', '$2y$10$SF.Jqh2wGZpT3KrFe.ydXORi0Oi1InBBogKHUM8YuBJ6HBxfsxrmO', NULL, '2017-08-11 20:35:26', '2017-08-11 20:35:26', 'kurikulum', 0, 0, 0, 0, 0, 0, 0),
(63, 'Yudi Hendrawati', '1386758476@sma2pskd.com', '$2y$10$tiZyEX1zlWNWBVJgQxLUA.EyzK/zSklxgi2.efcYQilmkzlBJbomK', 'ym9UvYhKWNkc9mQgFDPYZfBvQycUHQIPe1G0FJjYKVfiJSiabPuLV1y6FJY6', '2017-08-12 07:23:42', '2017-08-12 07:24:26', 'siswa', 1, 7, 0, 0, 0, 0, 0),
(64, 'Yustin Elisa', '1347873872@sma2pskd.com', '$2y$10$ZaAi0ZoiMavWjWmq4kavC.tM4m0facfN3onk0Kz3EfVLBxyzMef9a', NULL, '2017-08-12 07:25:25', '2017-08-12 07:25:25', 'siswa', 2, 8, 0, 0, 0, 0, 0),
(65, 'Sartik Dewi Sri', '13898554@sma2pskd.com', '$2y$10$JEYglj7jZtlarg6NEcxek.fKnkdqnDy1Y9TZfTp3xsf9E5JFK27py', NULL, '2017-08-12 07:26:16', '2017-08-12 07:26:16', 'siswa', 3, 9, 0, 0, 0, 0, 0),
(66, 'Waldiansyah Saputra', '1325546534@sma2pskd.com', '$2y$10$/EznpEngbidBOa9.Y71u1u9le.3ZkTm6kSoH4tGyLnN1z4NW46jgG', NULL, '2017-08-12 07:27:10', '2017-08-12 07:27:10', 'siswa', 4, 10, 0, 0, 0, 0, 0),
(67, 'Siska Siahaan', '1389878473@sma2pskd.com', '$2y$10$yp.u85X.UKNkYngu/Pll3Ol9dpKO6nz0vwjSUDHIdXqz/LCZGoLgC', NULL, '2017-08-12 07:28:14', '2017-08-12 07:28:14', 'siswa', 5, 11, 0, 0, 0, 0, 0),
(68, 'Listy Dwi Erna', '1396887867@sma2pskd.com', '$2y$10$JM9AIs4brFrEnEspesBCDu5SYNsxr7ksOxtAXDOKUi8uknTALofTa', NULL, '2017-08-12 07:30:40', '2017-08-12 07:30:40', 'siswa', 8, 12, 0, 0, 0, 0, 0),
(69, 'Netti Budiarti', '1311243212@sma2pskd.com', '$2y$10$v2JbzNy4u6u9EPsM2uoWCuXK.jFChbnMWm8COXkD9HO1..2guZ1Oq', NULL, '2017-08-12 07:31:29', '2017-08-12 07:31:29', 'siswa', 9, 13, 0, 0, 0, 0, 0),
(70, 'Sabah Barus', '1396885748@sma2pskd.com', '$2y$10$v8d750OdHCePzGM7xqFtcOqF6NXodMuBPGwLwwTQI/Q47hEjn5Q6i', NULL, '2017-08-12 07:32:04', '2017-08-12 07:32:04', 'siswa', 10, 14, 0, 0, 0, 0, 0),
(71, 'Risma Samosir', '1389786785@sma2pskd.com', '$2y$10$AOiIz9uDxDTQhqp0P9HSRe4HeA7sHygE6f1AejuBeKUlc4LXgd5PK', NULL, '2017-08-12 07:32:42', '2017-08-12 07:32:42', 'siswa', 11, 15, 0, 0, 0, 0, 0),
(72, 'Mangasi Situmorang', '1390789586@sma2pskd.com', '$2y$10$bxr41sJWj5T.NDVF7I9G1uuT6oMbK6gn4f95M0VRgh27pmv/GgSYO', NULL, '2017-08-12 07:33:24', '2017-08-12 07:33:24', 'siswa', 12, 16, 0, 0, 0, 0, 0),
(73, 'Rifai Ferdiansyah', '1398883092@sma2pskd.com', '$2y$10$QrqQdfexJuOoJ5i7dpW25.IeUODKKKGk3i.h5bIRT.8T4ZHKue1C6', NULL, '2017-08-12 07:34:22', '2017-08-12 07:34:22', 'siswa', 13, 17, 0, 0, 0, 0, 0),
(74, 'Rusmina Pardede', '1389770352@sma2pskd.com', '$2y$10$OJlzKqg39UGD3TbVkB.MXuqYaqUIFnt8cotkNpb7xXklXiufP77Yi', NULL, '2017-08-12 07:35:04', '2017-08-12 07:35:04', 'siswa', 14, 18, 0, 0, 0, 0, 0),
(75, 'Lim Herniati', '1387998374@sma2pskd.com', '$2y$10$iICshOuRb9s/LsH8e.EG2OqFNDyiX0.wJV.FtDElHJPytP6NymN2K', NULL, '2017-08-12 07:36:18', '2017-08-12 07:36:18', 'siswa', 15, 19, 0, 0, 0, 0, 0),
(76, 'Endah Sari', '1325489687@sma2pskd.com', '$2y$10$RW7QKxpgwYa6ns.chPZpnuBj8kEhLAXzMDiwbBfpwA5ZCi7h7uHai', NULL, '2017-08-12 07:37:01', '2017-08-12 07:37:01', 'siswa', 16, 20, 0, 0, 0, 0, 0),
(77, 'Benan Sitinjak', '1389777468@sma2pskd.com', '$2y$10$bBa43oxWlzsLPUXvxP.CF.OZOzGUpXkADRKGYMWHGRZgU9rTC2/BW', NULL, '2017-08-12 07:37:56', '2017-08-12 07:37:56', 'siswa', 17, 21, 0, 0, 0, 0, 0),
(78, 'Nuel Siahaan', '1348867857@sma2pskd.com', '$2y$10$FG3hLmFIj0y6mmzfyHwGTeh97OAfNIRHTVLveKWUimXqQiw8T3EE.', NULL, '2017-08-12 07:38:38', '2017-08-12 07:38:38', 'siswa', 18, 22, 0, 0, 0, 0, 0),
(79, 'Andi Nurmansyar', '1390567764@sma2pskd.com', '$2y$10$gQIdosOmhva431laHUr2.eU2hWYlgsDcrgL.XYGuvBIJvJtvYFo2a', 'eLHCN8DqxGdKQ2SUM9Kh91IS50J0y5mtaSHTPShoNc4LBhasC9Zj4OQaSwAX', '2017-08-12 07:39:33', '2017-08-29 20:02:41', 'guru', 19, 23, 0, 0, 0, 0, 0),
(80, 'Endang Dermawan', '134886785@sma2pskd.com', '$2y$10$othM.DSaXgx8SdlVuL72MuCv7AUxIAMgtkpA2.8zMS5UEBE73BrnG', 'th4CMrnjKMzZ2QRaHu0CqnaqOBMYhI3JI45ejGiT1PTjroOFQTmHk9Sgx1KI', '2017-08-12 07:40:16', '2017-09-12 21:55:50', 'guru', 20, 24, 0, 0, 0, 0, 0),
(81, 'Imas Eliana', '1358977584@sma2pskd.com', '$2y$10$ZpaZja6Cuy96sw2BQciPFuGfxEGR7/QIYoFGvvBEoPEktVlqVDqbm', NULL, '2017-08-12 07:41:02', '2017-08-12 07:41:02', 'siswa', 21, 25, 0, 0, 0, 0, 0),
(82, 'Samsul Subekti', '1389786764@sma2pskd.com', '$2y$10$crmGsmk99EjbKBK8AN6TsODQAHek4svRTFqremVG4/cJ45ZF2g9De', NULL, '2017-08-12 07:42:41', '2017-08-12 07:42:41', 'siswa', 22, 26, 0, 0, 0, 0, 0),
(83, '7', '1386758476.kurikulum@sma2pskd.com', '$2y$10$ttyCbwYuuF6Sdyz.GKB.zuMNHUOx1l30W5OHspGFk.eGJA95DXtUq', 'XKJGUvPLtDyyaglh4dZjBIoR5YyKQf7KFK89CnGtWPOjX3YVaqrZlrNJVENw', '2017-08-12 07:51:48', '2017-09-11 08:39:32', 'kurikulum', 0, 0, 0, 16, 0, 0, 0),
(86, '', 'santi@sma2pskd.com', '$2y$10$7e1vRC6mbLPlQYDIeeTqxOFk37xiZUuFp491OGDCgSUuhrWPTGst6', NULL, '2017-08-15 08:07:36', '2017-08-15 08:07:36', 'keuangan', 0, 0, 0, 0, 0, 0, 0),
(87, 'Aldiansyah Putra', 'aldiansyah@sma2pskd.com', '$2y$10$jmHeJ8sblwztSIzIhe3ltuLD2FOLGHrqq0vPRpx06nYPKUdG8wZ0C', 'zezGJ5V3GoB1DXRZJvtQG3CfiX7kAyMV18nDtiby4IsNfpTQojHhxfIdWR73', '2017-08-15 08:14:25', '2017-09-12 22:01:06', 'keuangan', 0, 0, 0, 0, 0, 4, 0),
(88, '', 'coba@sma2pskd.com', '$2y$10$E9xGBoP4Ugd3E/ZAzTJtoOkASn6Ydnijv5sQSa.CF6qyMgINXhXXW', NULL, '2017-08-15 22:46:19', '2017-08-15 22:46:19', 'keuangan', 0, 0, 0, 0, 0, 0, 0),
(89, 'Ari Kristianti', '1356766738.kepsek@sma2pskd.com', '$2y$10$Ztf0ByBIgHGCC997KfuLi.mNgfiwyLU650.qLxxn8ZZx/txj2M4QO', '9Tv2ZoiJ4mi5sN3ZZ6vSVRAoxuQf3cnRpsAqPeZfWq0hPbYvFxDCrFcA0Vn9', '2017-08-16 23:41:49', '2017-09-12 21:58:53', 'kepsek', 0, 0, 0, 0, 1, 0, 0),
(90, '', '201730040@sma2pskd.com', '$2y$10$bsZmkq.My69WyscY0lN90OaY7MBEpeV4f9oxxQEBzOa5qH/J0r.OK', NULL, '2017-09-09 06:10:39', '2017-09-09 06:10:39', 'siswa', 0, 0, 40, 0, 0, 0, 0),
(91, '', '201730041@sma2pskd.com', '$2y$10$4a/LgRHtZkzUlIAWujQ1be0wqHKEzPAXUVjeE6KYPzD4Pik1TBVBS', NULL, '2017-09-09 06:11:51', '2017-09-09 06:11:51', 'siswa', 0, 0, 41, 0, 0, 0, 0),
(92, '9', '13898554.kurikulum@sma2pskd.com', '$2y$10$DYXCmnwv0/fvAteKGdrnH.H/gOaZbOkHJkTQwDYkvN4cxCl8hlsZe', 'GRiTDM1eZ5LUAsrGLCYWniJqOpLDLxZhS7RmvuHZ0yIi1MavaxCCxVgddcXg', '2017-09-12 03:53:32', '2017-09-12 04:00:34', 'kurikulum', 0, 0, 0, 0, 0, 0, 0),
(93, 'Coba Baru', '1346998574@sma2pskd.com', '$2y$10$891nIVRblCExQSqWBowCQuREG9OyeMkOKgDh62kUZz4mq9q.6XI5i', 'TwORitWcVhHvMEeeXUh8l2TuQVxi9G1vFU3Fbo4bY3xFntdM2nfYLC7ryEMS', '2017-09-12 04:01:37', '2017-09-12 04:02:03', 'guru', 0, 27, 0, 0, 0, 0, 0),
(94, 'baru ajaa', '201730042@sma2pskd.com', '$2y$10$BXNGSvkcT/hnNkNKvX3r/.JEgNK8Y2B/UXUXyAiUgdKX7OUpXbdBS', 'iiICWOSqWMSeBvonH4ZFHxflGHI0boV87h1q3iExIYU0rLxk1w89X0MAewqR', '2017-09-12 04:04:06', '2017-09-12 04:05:54', 'siswa', 0, 0, 42, 0, 0, 0, 0),
(95, '', 'Imanuel@sma2pskd.com', '$2y$10$xuRxMRQamlJu1aZxdpewIugP5v2aowOGabvrEbYyUtxSdo4TgvxdW', '69uO180aUG64Pp7qApP80pT15ucrdNsdC2POZqRplXIRS90EIAjILIxQTwyf', '2017-09-12 04:06:31', '2017-09-12 04:07:52', 'keuangan', 0, 0, 0, 0, 0, 0, 0),
(96, '', 'ddd@sma2pskd.com', '$2y$10$a7LfF5nZKm4J2G3caVVFiubIeIgLZCoefqot2UUTXRuI/ElVOErMS', 'w9s9F68hW0xat9FWFemr84HS52eeE9nAu0XUlFSqkdeqfuKZN822pDZRrLpZ', '2017-09-12 04:08:16', '2017-09-12 04:09:15', 'keuangan', 0, 0, 0, 0, 0, 0, 0),
(100, '26', '1389786764.kurikulum@sma2pskd.com', '$2y$10$/SQCPsYwA/nPYqaNpRuMD.4KjF5X7qYCVH1TrIni9qo4Nj6gcgtqW', NULL, '2017-09-12 08:30:22', '2017-09-12 08:30:22', 'kurikulum', 0, 0, 0, 23, 0, 0, 0),
(103, '', 'ads@sma2pskd.com', '$2y$10$urhIripTy6Us9nJoPODRV.8XgHocH7jOh6cyZSWOljBwrkNGM8lai', NULL, '2017-09-12 09:30:42', '2017-09-12 09:30:42', 'keuangan', 0, 0, 0, 0, 0, 9, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absen`
--
ALTER TABLE `absen`
  ADD PRIMARY KEY (`absen_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`guru_id`);

--
-- Indexes for table `jadwal_pelajaran`
--
ALTER TABLE `jadwal_pelajaran`
  ADD PRIMARY KEY (`jadwal_id`);

--
-- Indexes for table `kalender`
--
ALTER TABLE `kalender`
  ADD PRIMARY KEY (`id_kalender`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `kepsek`
--
ALTER TABLE `kepsek`
  ADD PRIMARY KEY (`id_kepsek`);

--
-- Indexes for table `keuangan`
--
ALTER TABLE `keuangan`
  ADD PRIMARY KEY (`id_keuangan`);

--
-- Indexes for table `keuangansiswa`
--
ALTER TABLE `keuangansiswa`
  ADD PRIMARY KEY (`id_bayaran`);

--
-- Indexes for table `kurikulum`
--
ALTER TABLE `kurikulum`
  ADD PRIMARY KEY (`id_kurikulum`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`mapel_id`);

--
-- Indexes for table `mapel_map`
--
ALTER TABLE `mapel_map`
  ADD PRIMARY KEY (`id_map`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`nilai_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`siswa_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absen`
--
ALTER TABLE `absen`
  MODIFY `absen_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `guru_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `jadwal_pelajaran`
--
ALTER TABLE `jadwal_pelajaran`
  MODIFY `jadwal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `kalender`
--
ALTER TABLE `kalender`
  MODIFY `id_kalender` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kepsek`
--
ALTER TABLE `kepsek`
  MODIFY `id_kepsek` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `keuangan`
--
ALTER TABLE `keuangan`
  MODIFY `id_keuangan` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `keuangansiswa`
--
ALTER TABLE `keuangansiswa`
  MODIFY `id_bayaran` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `kurikulum`
--
ALTER TABLE `kurikulum`
  MODIFY `id_kurikulum` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `mapel_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `mapel_map`
--
ALTER TABLE `mapel_map`
  MODIFY `id_map` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `nilai_id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `siswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
