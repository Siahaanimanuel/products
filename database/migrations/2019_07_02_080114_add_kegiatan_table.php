<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kegiatan', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('nama_kegiatan', 255)->primary()->unique();
            $table->string('tempat', 255);
            $table->string('tanggal',255);
            $table->string('isi',255);
            $table->string('presideum', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kegiatan');
    }
}
