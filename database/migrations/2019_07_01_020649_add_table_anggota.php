<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('anggota_id', 36)->primary()->unique();
            $table->string('nama_depan', 36);
            $table->string('nama_belakang',36);
            $table->string('talenta',36);
            $table->string('jenis_kelamin', 36);
            $table->string('perguruan_tinggi',36);
            $table->string('tempat_lahir',36);
            $table->string('tgl_lahir', 36);
            $table->string('alamat1',36);
            $table->string('alamat2',36);
            $table->string('kode_pos', 36);
            $table->string('kota',36);
            $table->string('provinsi',36);
            $table->string('instagram', 36);
            $table->string('telepon',36);
            $table->string('gereja',36);
            $table->string('foto', 36);
            $table->string('ktp',36);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('anggota');
    }
}
